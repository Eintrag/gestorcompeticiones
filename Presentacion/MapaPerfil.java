package Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.border.TitledBorder;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JScrollPane;

public class MapaPerfil extends JFrame {

	private JPanel contentPane;
	private JPanel panel;
	private JButton button;
	private JComboBox comboBox;
	private JLabel lblDificultadDelTramo;
	private JScrollPane scrollPane;
	private JLabel label;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MapaPerfil frame = new MapaPerfil();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MapaPerfil() {
		setTitle(Messages.getString("MapaPerfil.0")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 616, 494);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		{
			panel = new JPanel();
			panel.setBorder(new EmptyBorder(5, 5, 5, 5));
			contentPane.add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 126, 0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				lblDificultadDelTramo = new JLabel(Messages.getString("MapaPerfil.1")); //$NON-NLS-1$
				GridBagConstraints gbc_lblDificultadDelTramo = new GridBagConstraints();
				gbc_lblDificultadDelTramo.anchor = GridBagConstraints.WEST;
				gbc_lblDificultadDelTramo.insets = new Insets(0, 0, 5, 0);
				gbc_lblDificultadDelTramo.gridx = 3;
				gbc_lblDificultadDelTramo.gridy = 1;
				panel.add(lblDificultadDelTramo, gbc_lblDificultadDelTramo);
			}
			{
				scrollPane = new JScrollPane();
				GridBagConstraints gbc_scrollPane = new GridBagConstraints();
				gbc_scrollPane.gridheight = 2;
				gbc_scrollPane.gridwidth = 3;
				gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
				gbc_scrollPane.fill = GridBagConstraints.BOTH;
				gbc_scrollPane.gridx = 0;
				gbc_scrollPane.gridy = 1;
				panel.add(scrollPane, gbc_scrollPane);
				{
					label = new JLabel(""); //$NON-NLS-1$
					label.setIcon(new ImageIcon(MapaPerfil.class.getResource("/resources/Perfil Carrera.jpg"))); //$NON-NLS-1$
					scrollPane.setViewportView(label);
				}
			}
			{
				comboBox = new JComboBox();
				comboBox.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("MapaPerfil.2"), Messages.getString("MapaPerfil.3"), Messages.getString("MapaPerfil.4")})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				GridBagConstraints gbc_comboBox = new GridBagConstraints();
				gbc_comboBox.anchor = GridBagConstraints.NORTH;
				gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
				gbc_comboBox.insets = new Insets(0, 0, 5, 0);
				gbc_comboBox.gridx = 3;
				gbc_comboBox.gridy = 2;
				panel.add(comboBox, gbc_comboBox);
			}
			{
				button = new JButton(Messages.getString("MapaPerfil.5")); //$NON-NLS-1$
				button.addActionListener(new ButtonActionListener());
				GridBagConstraints gbc_button = new GridBagConstraints();
				gbc_button.insets = new Insets(0, 0, 5, 0);
				gbc_button.gridx = 3;
				gbc_button.gridy = 3;
				panel.add(button, gbc_button);
			}
		}
	}

	private class ButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			Mapa ventanaMapa=new Mapa();
			//se hace visible
			ventanaMapa.setVisible(true);
			//se destruye la ventana actual (atributo a nivel de clase)
			dispose();
		}
	}
}
