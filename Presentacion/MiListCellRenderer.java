package Presentacion;
import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;
class MiListCellRenderer extends DefaultListCellRenderer {
	protected DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
	public Component getListCellRendererComponent(JList list, Object value, int index,boolean isSelected, boolean hasFocus) {
		JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
				isSelected, hasFocus);

		renderer.setBorder(new TitledBorder(LineBorder.createGrayLineBorder(),""));
		renderer.setHorizontalAlignment(JLabel.CENTER);
		renderer.setBackground(new Color(250,250,245));
		renderer.setForeground(Color.BLUE);
		if (isSelected) renderer.setBackground(new Color(200,250,200));
		return renderer;
	}
}