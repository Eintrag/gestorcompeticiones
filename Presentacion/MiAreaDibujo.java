package Presentacion;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.JLabel;

public class MiAreaDibujo extends JLabel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<ObjetoGrafico> objetosGraficos = new ArrayList<ObjetoGrafico>();

	public MiAreaDibujo(){
	}
	void clear(){
		objetosGraficos = new ArrayList<ObjetoGrafico>();	
		}
	void addObjetoGrafico(ObjetoGrafico imagenGrafico) {
		objetosGraficos.add(imagenGrafico);
	}
	public ObjetoGrafico getUltimoObjetoGrafico()
	{
		return objetosGraficos.get(objetosGraficos.size()-1);
	}
	public void paint(Graphics g){
		super.paint(g);

		//T21
		for (int i = 0; i < objetosGraficos.size(); i++) {
			ObjetoGrafico objg = objetosGraficos.get(i);
			if (objg instanceof ImagenGrafico)
			{
				g.drawImage(((ImagenGrafico)objg).getImagen(), objg.getX(), objg.getY(), null);
			}
		} 
		//T21
	}
}
