package Presentacion;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JScrollPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.border.LineBorder;

import java.awt.Color;

import javax.swing.border.TitledBorder;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.ListSelectionModel;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.AbstractListModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.text.ParseException;

import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.text.MaskFormatter;
import javax.swing.JToggleButton;
import javax.swing.ImageIcon;

public class Participantes extends JFrame {

	private JPanel contentPane;
	private JScrollPane scrollPane;
	private JButton btnNuevo;
	private JButton btnBorrar;
	private JList lstParticipantes;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JButton btnEnviar;
	private JLabel lblTallaDeCamiseta;
	private JComboBox cbTalla;
	private JLabel lblSponsor;
	private JLabel lblClub;
	private JTextPane txtpnMensaje;
	private JLabel lblFoto;
	private JTextField txtNombre;
	private JLabel lblNacimiento;
	private JLabel lblDni;
	private JLabel lblTlf;
	private JFormattedTextField ftxtNacimiento;
	private DefaultListModel modeloListaPart;
	
	private int seleccion;
	private JToggleButton btnModificar;
	private JFormattedTextField ftxtDni;
	private JFormattedTextField ftxtTlf;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Participantes frame = new Participantes();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Participantes() {
		addWindowListener(new ThisWindowListener());
		setTitle(Messages.getString("Participantes.this.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 632, 474);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0, 0, 86, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		{
			scrollPane = new JScrollPane();
			GridBagConstraints gbc_scrollPane = new GridBagConstraints();
			gbc_scrollPane.gridheight = 4;
			gbc_scrollPane.gridwidth = 3;
			gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
			gbc_scrollPane.fill = GridBagConstraints.BOTH;
			gbc_scrollPane.gridx = 1;
			gbc_scrollPane.gridy = 0;
			contentPane.add(scrollPane, gbc_scrollPane);
			
			lstParticipantes = new JList();
			lstParticipantes.addListSelectionListener(new LstParticipantesListSelectionListener());
			lstParticipantes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			lstParticipantes.setCellRenderer(new MiListCellRenderer());
			
			modeloListaPart = new DefaultListModel();
			lstParticipantes.setModel(modeloListaPart);
			
			modeloListaPart.addElement("López Gómez, Manuel"); //$NON-NLS-1$
			modeloListaPart.addElement("Trujillo Peña, Clara"); //$NON-NLS-1$
			modeloListaPart.addElement("Ramírez Alas, Alfonso"); //$NON-NLS-1$
			lstParticipantes.setSelectedIndex(0);
			scrollPane.setViewportView(lstParticipantes);
			/*
			{
				list = new JList();
				list.addListSelectionListener(new ListListSelectionListener());
				list.setModel(new AbstractListModel() {
					String[] values = new String[] {"López Gómez, Manuel", "Trujillo Peña, Clara"};
					public int getSize() {
						return values.length;
					}
					public Object getElementAt(int index) {
						return values[index];
					}
				});
				scrollPane.setViewportView(list);
			}
			*/
		}
		{
			panel = new JPanel();
			panel.setBorder(new LineBorder(new Color(0, 0, 0)));
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 5;
			gbc_panel.gridy = 0;
			contentPane.add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{6, 0};
			gbl_panel.rowHeights = new int[]{20, 0};
			gbl_panel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				txtpnMensaje = new JTextPane();
				txtpnMensaje.setText(Messages.getString("Participantes.txtpnMensaje.text")); //$NON-NLS-1$
				GridBagConstraints gbc_txtpnMensaje = new GridBagConstraints();
				gbc_txtpnMensaje.fill = GridBagConstraints.BOTH;
				gbc_txtpnMensaje.gridx = 0;
				gbc_txtpnMensaje.gridy = 0;
				panel.add(txtpnMensaje, gbc_txtpnMensaje);
			}
		}
		{
			btnEnviar = new JButton(Messages.getString("Participantes.btnEnviar.text")); //$NON-NLS-1$
			btnEnviar.addActionListener(new BtnEnviarActionListener());
			GridBagConstraints gbc_btnEnviar = new GridBagConstraints();
			gbc_btnEnviar.anchor = GridBagConstraints.EAST;
			gbc_btnEnviar.insets = new Insets(0, 0, 5, 0);
			gbc_btnEnviar.gridx = 5;
			gbc_btnEnviar.gridy = 1;
			contentPane.add(btnEnviar, gbc_btnEnviar);
		}
		{
			panel_2 = new JPanel();
			panel_2.setBorder(new TitledBorder(null, Messages.getString("Participantes.panel_2.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
			GridBagConstraints gbc_panel_2 = new GridBagConstraints();
			gbc_panel_2.insets = new Insets(0, 0, 5, 0);
			gbc_panel_2.fill = GridBagConstraints.BOTH;
			gbc_panel_2.gridx = 5;
			gbc_panel_2.gridy = 2;
			contentPane.add(panel_2, gbc_panel_2);
			GridBagLayout gbl_panel_2 = new GridBagLayout();
			gbl_panel_2.columnWidths = new int[]{75, 17, 0, 0, 0};
			gbl_panel_2.rowHeights = new int[]{14, 0, 0, 0, 0};
			gbl_panel_2.columnWeights = new double[]{1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			gbl_panel_2.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel_2.setLayout(gbl_panel_2);
			{
				lblFoto = new JLabel(""); //$NON-NLS-1$
				lblFoto.setIcon(new ImageIcon(Participantes.class.getResource("/resources/awesomeFace.jpeg"))); //$NON-NLS-1$
				GridBagConstraints gbc_lblFoto = new GridBagConstraints();
				gbc_lblFoto.gridheight = 4;
				gbc_lblFoto.insets = new Insets(0, 0, 0, 5);
				gbc_lblFoto.anchor = GridBagConstraints.NORTHEAST;
				gbc_lblFoto.gridx = 0;
				gbc_lblFoto.gridy = 0;
				panel_2.add(lblFoto, gbc_lblFoto);
			}
			{
				txtNombre = new JTextField();
				txtNombre.setEditable(false);
				txtNombre.setText(lstParticipantes.getSelectedValue().toString());
				GridBagConstraints gbc_txtNombre = new GridBagConstraints();
				gbc_txtNombre.gridwidth = 2;
				gbc_txtNombre.insets = new Insets(0, 0, 5, 0);
				gbc_txtNombre.fill = GridBagConstraints.HORIZONTAL;
				gbc_txtNombre.gridx = 2;
				gbc_txtNombre.gridy = 0;
				panel_2.add(txtNombre, gbc_txtNombre);
				txtNombre.setColumns(10);
			}
			{
				lblNacimiento = new JLabel(Messages.getString("Participantes.lblNacimiento.text")); //$NON-NLS-1$
				GridBagConstraints gbc_lblNacimiento = new GridBagConstraints();
				gbc_lblNacimiento.anchor = GridBagConstraints.EAST;
				gbc_lblNacimiento.insets = new Insets(0, 0, 5, 5);
				gbc_lblNacimiento.gridx = 2;
				gbc_lblNacimiento.gridy = 1;
				panel_2.add(lblNacimiento, gbc_lblNacimiento);
			}
			{
				MaskFormatter formatoFecha;
				try {
					formatoFecha = new MaskFormatter("##-##-####'"); //$NON-NLS-1$
					formatoFecha.setPlaceholderCharacter('X');
					ftxtNacimiento = new JFormattedTextField(formatoFecha);
					ftxtNacimiento.setEditable(false);
				}catch (ParseException e){
					e.printStackTrace();
				}
				ftxtNacimiento.setText("20-08-1995"); //$NON-NLS-1$
				GridBagConstraints gbc_ftxtNacimiento = new GridBagConstraints();
				gbc_ftxtNacimiento.insets = new Insets(0, 0, 5, 0);
				gbc_ftxtNacimiento.fill = GridBagConstraints.HORIZONTAL;
				gbc_ftxtNacimiento.gridx = 3;
				gbc_ftxtNacimiento.gridy = 1;
				panel_2.add(ftxtNacimiento, gbc_ftxtNacimiento);
			}
			{
				lblDni = new JLabel("DNI:"); //$NON-NLS-1$
				GridBagConstraints gbc_lblDni = new GridBagConstraints();
				gbc_lblDni.anchor = GridBagConstraints.EAST;
				gbc_lblDni.insets = new Insets(0, 0, 5, 5);
				gbc_lblDni.gridx = 2;
				gbc_lblDni.gridy = 2;
				panel_2.add(lblDni, gbc_lblDni);
			}
			{
				MaskFormatter formatoDNI;
				try {
					formatoDNI = new MaskFormatter("########' -U"); //$NON-NLS-1$
					formatoDNI.setPlaceholderCharacter('X');
					ftxtDni = new JFormattedTextField(formatoDNI);
					ftxtDni.setEditable(false);
					ftxtDni.setText("51234567 -X"); //$NON-NLS-1$
				}catch (ParseException e){
					e.printStackTrace();
				}
				GridBagConstraints gbc_ftxtDni = new GridBagConstraints();
				gbc_ftxtDni.insets = new Insets(0, 0, 5, 0);
				gbc_ftxtDni.fill = GridBagConstraints.HORIZONTAL;
				gbc_ftxtDni.gridx = 3;
				gbc_ftxtDni.gridy = 2;
				panel_2.add(ftxtDni, gbc_ftxtDni);
			}
			{
				lblTlf = new JLabel(Messages.getString("Participantes.lblTlf.text")); //$NON-NLS-1$
				GridBagConstraints gbc_lblTlf = new GridBagConstraints();
				gbc_lblTlf.anchor = GridBagConstraints.EAST;
				gbc_lblTlf.insets = new Insets(0, 0, 0, 5);
				gbc_lblTlf.gridx = 2;
				gbc_lblTlf.gridy = 3;
				panel_2.add(lblTlf, gbc_lblTlf);
			}
			{
				MaskFormatter formatoTlfno;
				try {
					formatoTlfno = new MaskFormatter("'(###')' ###' ###' ###"); //$NON-NLS-1$
					formatoTlfno.setPlaceholderCharacter('*');
					ftxtTlf = new JFormattedTextField(formatoTlfno);
					ftxtTlf.setEditable(false);
					ftxtTlf.setText("034 688 999 333"); //$NON-NLS-1$
				}catch (ParseException e){
					e.printStackTrace();
				}
				GridBagConstraints gbc_ftxtTlf = new GridBagConstraints();
				gbc_ftxtTlf.fill = GridBagConstraints.HORIZONTAL;
				gbc_ftxtTlf.gridx = 3;
				gbc_ftxtTlf.gridy = 3;
				panel_2.add(ftxtTlf, gbc_ftxtTlf);
			}
		}
		{
			panel_1 = new JPanel();
			panel_1.setBorder(new TitledBorder(null, Messages.getString("Participantes.panel_1.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
			GridBagConstraints gbc_panel_1 = new GridBagConstraints();
			gbc_panel_1.gridheight = 2;
			gbc_panel_1.insets = new Insets(0, 0, 5, 0);
			gbc_panel_1.fill = GridBagConstraints.BOTH;
			gbc_panel_1.gridx = 5;
			gbc_panel_1.gridy = 3;
			contentPane.add(panel_1, gbc_panel_1);
			GridBagLayout gbl_panel_1 = new GridBagLayout();
			gbl_panel_1.columnWidths = new int[]{0, 0, 0};
			gbl_panel_1.rowHeights = new int[]{0, 0, 0, 0};
			gbl_panel_1.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
			gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel_1.setLayout(gbl_panel_1);
			{
				lblTallaDeCamiseta = new JLabel(Messages.getString("Participantes.lblTallaDeCamiseta.text")); //$NON-NLS-1$
				GridBagConstraints gbc_lblTallaDeCamiseta = new GridBagConstraints();
				gbc_lblTallaDeCamiseta.insets = new Insets(0, 0, 5, 5);
				gbc_lblTallaDeCamiseta.anchor = GridBagConstraints.EAST;
				gbc_lblTallaDeCamiseta.gridx = 0;
				gbc_lblTallaDeCamiseta.gridy = 0;
				panel_1.add(lblTallaDeCamiseta, gbc_lblTallaDeCamiseta);
			}
			{
				cbTalla = new JComboBox();
				cbTalla.setModel(new DefaultComboBoxModel(new String[] {"XS", "S", "M", "L", "XL", "XXL"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$
				GridBagConstraints gbc_cbTalla = new GridBagConstraints();
				cbTalla.addFocusListener(new MiComboBoxFocusListener());
				cbTalla.addActionListener(new MiComboBoxActionListener());
				gbc_cbTalla.insets = new Insets(0, 0, 5, 0);
				gbc_cbTalla.fill = GridBagConstraints.HORIZONTAL;
				gbc_cbTalla.gridx = 1;
				gbc_cbTalla.gridy = 0;
				panel_1.add(cbTalla, gbc_cbTalla);
			}
			{
				lblSponsor = new JLabel("Sponsor"); //$NON-NLS-1$
				GridBagConstraints gbc_lblSponsor = new GridBagConstraints();
				gbc_lblSponsor.insets = new Insets(0, 0, 0, 5);
				gbc_lblSponsor.gridx = 0;
				gbc_lblSponsor.gridy = 2;
				panel_1.add(lblSponsor, gbc_lblSponsor);
			}
			{
				lblClub = new JLabel("Club"); //$NON-NLS-1$
				GridBagConstraints gbc_lblClub = new GridBagConstraints();
				gbc_lblClub.gridx = 1;
				gbc_lblClub.gridy = 2;
				panel_1.add(lblClub, gbc_lblClub);
			}
		}
		{
			btnNuevo = new JButton(Messages.getString("Participantes.btnNuevo.text")); //$NON-NLS-1$
			btnNuevo.addActionListener(new BtnNuevoActionListener());
			GridBagConstraints gbc_btnNuevo = new GridBagConstraints();
			gbc_btnNuevo.insets = new Insets(0, 0, 5, 5);
			gbc_btnNuevo.gridx = 1;
			gbc_btnNuevo.gridy = 4;
			contentPane.add(btnNuevo, gbc_btnNuevo);
		}
		{
			btnModificar = new JToggleButton(Messages.getString("Participantes.btnModificar.text")); //$NON-NLS-1$
			btnModificar.addActionListener(new BtnModificarActionListener());
			GridBagConstraints gbc_btnModificar = new GridBagConstraints();
			gbc_btnModificar.insets = new Insets(0, 0, 5, 5);
			gbc_btnModificar.gridx = 2;
			gbc_btnModificar.gridy = 4;
			contentPane.add(btnModificar, gbc_btnModificar);
		}
		{
			btnBorrar = new JButton(Messages.getString("Participantes.btnBorrar.text")); //$NON-NLS-1$
			btnBorrar.addActionListener(new BtnBorrarActionListener());
			GridBagConstraints gbc_btnBorrar = new GridBagConstraints();
			gbc_btnBorrar.fill = GridBagConstraints.HORIZONTAL;
			gbc_btnBorrar.insets = new Insets(0, 0, 5, 5);
			gbc_btnBorrar.gridx = 3;
			gbc_btnBorrar.gridy = 4;
			contentPane.add(btnBorrar, gbc_btnBorrar);
		}
	}

	private class ThisWindowListener extends WindowAdapter {
		@Override
		public void windowClosed(WindowEvent arg0) {
			
			
		}
	}
	private class BtnEnviarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			JOptionPane.showMessageDialog(btnEnviar, Messages.getString("Participantes.0"), Messages.getString("Participantes.1"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}

	private class MiComboBoxActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			try{
				if (!btnModificar.isSelected() && ((JComboBox) arg0.getSource()).getSelectedIndex()!=seleccion){
				((JComboBox) arg0.getSource()).setSelectedIndex(seleccion);
				JOptionPane.showMessageDialog(null,Messages.getString("Participantes.2") //$NON-NLS-1$
						+ Messages.getString("Participantes.3"), Messages.getString("Participantes.4"),JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}catch (Exception exc){
				
			}
			
		}
	}
	private class MiComboBoxFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent e) {
			seleccion = ((JComboBox) e.getSource()).getSelectedIndex();
		}
	}
	private class BtnBorrarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
		  if(lstParticipantes.getSelectedValue()!=null){
			if(JOptionPane.showConfirmDialog(null, Messages.getString("Participantes.5") //$NON-NLS-1$
					 + lstParticipantes.getSelectedValue() +"?",Messages.getString("Participantes.6"), //$NON-NLS-1$ //$NON-NLS-2$
					JOptionPane.YES_NO_OPTION ) == 0){
				modeloListaPart.remove(lstParticipantes.getSelectedIndex());
				JOptionPane.showMessageDialog(null, Messages.getString("Participantes.7") //$NON-NLS-1$
						+ Messages.getString("Participantes.8"), Messages.getString("Participantes.9"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else{
				JOptionPane.showMessageDialog(null, Messages.getString("Participantes.10") //$NON-NLS-1$
						+ Messages.getString("Participantes.11"), Messages.getString("Participantes.12"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}
		  }
		  else  JOptionPane.showMessageDialog(null, Messages.getString("Participantes.13") //$NON-NLS-1$
				+ Messages.getString("Participantes.14"),  //$NON-NLS-1$
				Messages.getString("Participantes.15"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
		}
	}
	private class BtnNuevoActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String nombre = ""; //$NON-NLS-1$
			nombre = JOptionPane.showInputDialog(null, 
					Messages.getString("Participantes.16"),  //$NON-NLS-1$
					Messages.getString("Participantes.17"), JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
				
			if(nombre == null || nombre.equals("")){ //$NON-NLS-1$
				JOptionPane.showMessageDialog(null, Messages.getString("Participantes.18") //$NON-NLS-1$
						+ Messages.getString("Participantes.19"),  //$NON-NLS-1$
						Messages.getString("Participantes.20"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
				}
			else modeloListaPart.addElement(nombre);
				
			}
	}
	private class LstParticipantesListSelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent arg0) {
			try{
				boolean modificando = btnModificar.isSelected();
				btnModificar.setSelected(true);
				txtNombre.setText(lstParticipantes.getSelectedValue().toString());
				cargarParticipante();
				if (!modificando) btnModificar.setSelected(false);
			}catch (Exception exc){
			}
		}
	}
	private class BtnModificarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			boolean estado = btnModificar.isSelected();
			 ftxtDni.setEditable(estado);
			 ftxtTlf.setEditable(estado);
			 txtNombre.setEditable(estado);
			 ftxtNacimiento.setEditable(estado);
		}
	}
	public void cargarParticipante(){
		ftxtNacimiento.setText("13-06-1984"); //$NON-NLS-1$
		ftxtDni.setText("5312345 -V"); //$NON-NLS-1$
		ftxtTlf.setText("034 613 888 444"); //$NON-NLS-1$
		cbTalla.setSelectedIndex(2);
		lblFoto.setIcon(new ImageIcon(Principal.class.getResource("/resources/FeelLikeASir.jpeg"))); //$NON-NLS-1$

	}
}
