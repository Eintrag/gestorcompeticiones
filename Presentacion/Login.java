package Presentacion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Locale;

public class Login extends JFrame{
//En clase
	//hola chicos
	private JFrame frmLogin;
	private JPanel panel;
	private JButton btnEntrar;
	private JLabel lblUsuario;
	private JLabel lblContrasea;
	private JTextField textUsuario;
	private JToggleButton btnIngles;
	private JToggleButton btnEspanol;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Login window = new Login();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public JFrame getFrame(){
		return frmLogin;
	}
	public Login() {

		frmLogin = new JFrame();
		frmLogin.setTitle("Login"); //$NON-NLS-1$
		frmLogin.setBounds(100, 100, 357, 302);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setTitle("Login"); //$NON-NLS-1$
		//setBounds(100, 100, 357, 302);
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		{
			panel = new JPanel();
			frmLogin.getContentPane().add(panel, BorderLayout.CENTER);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{59, 112, 131, 35, 0};
			gbl_panel.rowHeights = new int[]{132, 0, 0, 47, 0};
			gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				btnIngles = new JToggleButton(Messages.getString("Login.btnIngles.text")); //$NON-NLS-1$
				btnIngles.addActionListener(new BtnInglesActionListener());
				btnIngles.setIcon(new ImageIcon(Login.class.getResource("/resources/United Kingdom FlagSMALL.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_btnIngles = new GridBagConstraints();
				gbc_btnIngles.insets = new Insets(0, 0, 5, 5);
				gbc_btnIngles.gridx = 1;
				gbc_btnIngles.gridy = 0;
				panel.add(btnIngles, gbc_btnIngles); 
			}
			{
				btnEspanol = new JToggleButton(Messages.getString("Login.btnEspanol.text")); //$NON-NLS-1$
				btnEspanol.addActionListener(new BtnEspanolActionListener());
				btnEspanol.setIcon(new ImageIcon(Login.class.getResource("/resources/Spain FlagSMALL.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_btnEspanol = new GridBagConstraints();
				gbc_btnEspanol.insets = new Insets(0, 0, 5, 5);
				gbc_btnEspanol.gridx = 2;
				gbc_btnEspanol.gridy = 0;
				panel.add(btnEspanol, gbc_btnEspanol);
			}
			Locale currentLocale = Locale.getDefault();
			String idioma = currentLocale.getLanguage();
			if (idioma.equals("en")){ //$NON-NLS-1$
				btnIngles.setSelected(true);
			}
			
			else if(idioma.equals("es")){ //$NON-NLS-1$
				btnEspanol.setSelected(true);
			}
			{
				{
					lblUsuario = new JLabel(Messages.getString("Login.lblUsuario.text")); //$NON-NLS-1$
					GridBagConstraints gbc_lblUsuario = new GridBagConstraints();
					gbc_lblUsuario.anchor = GridBagConstraints.WEST;
					gbc_lblUsuario.insets = new Insets(0, 0, 5, 5);
					gbc_lblUsuario.gridx = 1;
					gbc_lblUsuario.gridy = 1;
					panel.add(lblUsuario, gbc_lblUsuario);
				}
				{
					textUsuario = new JTextField();
					GridBagConstraints gbc_textUsuario = new GridBagConstraints();
					gbc_textUsuario.fill = GridBagConstraints.HORIZONTAL;
					gbc_textUsuario.insets = new Insets(0, 0, 5, 5);
					gbc_textUsuario.gridx = 2;
					gbc_textUsuario.gridy = 1;
					panel.add(textUsuario, gbc_textUsuario);
					textUsuario.setColumns(10);
				}
				{
					lblContrasea = new JLabel(Messages.getString("Login.lblContrasea.text")); //$NON-NLS-1$
					GridBagConstraints gbc_lblContrasea = new GridBagConstraints();
					gbc_lblContrasea.anchor = GridBagConstraints.WEST;
					gbc_lblContrasea.insets = new Insets(0, 0, 5, 5);
					gbc_lblContrasea.gridx = 1;
					gbc_lblContrasea.gridy = 2;
					panel.add(lblContrasea, gbc_lblContrasea);
				}
			}
			btnEntrar = new JButton(Messages.getString("Login.btnEntrar.text")); //$NON-NLS-1$
			btnEntrar.addActionListener(new ButtonActionListener());
			{
				passwordField = new JPasswordField();
				GridBagConstraints gbc_passwordField = new GridBagConstraints();
				gbc_passwordField.insets = new Insets(0, 0, 5, 5);
				gbc_passwordField.fill = GridBagConstraints.HORIZONTAL;
				gbc_passwordField.gridx = 2;
				gbc_passwordField.gridy = 2;
				panel.add(passwordField, gbc_passwordField);
			}
			GridBagConstraints gbc_btnEntrar = new GridBagConstraints();
			gbc_btnEntrar.fill = GridBagConstraints.BOTH;
			gbc_btnEntrar.insets = new Insets(0, 0, 0, 5);
			gbc_btnEntrar.gridwidth = 2;
			gbc_btnEntrar.gridx = 1;
			gbc_btnEntrar.gridy = 3;
			panel.add(btnEntrar, gbc_btnEntrar);
		}
	}
	private class ButtonActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			
			String usuario = textUsuario.getText();
			char[] contrasena = passwordField.getPassword();
			if (usuario.equals("")){ //$NON-NLS-1$
				JOptionPane.showMessageDialog(btnEntrar, Messages.getString("Login.TextoMessageDialogEntrar"), Messages.getString("Login.TituloMessageDialogEntrar"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else if (usuario.equals(Messages.getString("Login.0"))&& contrasena[0]=='D' && contrasena[1]=='o' && contrasena[2]=='g'){ //$NON-NLS-1$
				Principal ventanaPrincipal = new Principal(usuario);
				ventanaPrincipal.setVisible(true);
				frmLogin.dispose();
			}
			else {
				JOptionPane.showMessageDialog(null, Messages.getString("Login.1"), Messages.getString("Login.2"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}
		}
	}
	private class BtnEspanolActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			if (btnIngles.isSelected()){
				Locale.setDefault(new Locale("es")); //$NON-NLS-1$
				Messages.setIdioma("español"); //$NON-NLS-1$
				Login window = new Login();
				frmLogin.dispose();
				window.frmLogin.setVisible(true);
				//btnIngles.setSelected(false);	
			}
			if (!btnIngles.isSelected()){
				btnEspanol.setSelected(true);
			}
		}
	}
	private class BtnInglesActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			if (btnEspanol.isSelected()){
				Locale.setDefault(new Locale("en")); //$NON-NLS-1$
				Messages.setIdioma("inglés"); //$NON-NLS-1$
				Login window = new Login();
				frmLogin.dispose();
				window.frmLogin.setVisible(true);
				//btnEspanol.setSelected(false);	
			}
			if (!btnEspanol.isSelected()){
				btnIngles.setSelected(true);
			}
		}
	}
}
