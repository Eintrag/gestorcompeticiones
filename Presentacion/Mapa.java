package Presentacion;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;

import javax.swing.JLabel;

import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.border.TitledBorder;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;

public class Mapa extends JFrame {
	private JFrame frame;
	private JPanel contentPane;
	private JButton btnBorrar;
	private JPanel panel;
	private JButton btnMostrarPerfil;
	
	private final int START = 1;
	private final int META = 2;

	private final int DORSAL = 3;
	private final int BEBIDA = 4;
	private final int COMIDA = 5;
	private final int SOS = 6;
	private final int INTERES = 7;
	private JScrollPane scrollPane;
	private MiAreaDibujo miAreaDibujo;
	private ImageIcon imagen;
	
	private Toolkit toolkit;
	private Image imagStart;
	private Image imagMeta;
	private Image imagBebida;
	private Image imagComida;
	private Image imagDorsal;
	private Image imagSOS;
	private Image imagInteres;

	
	private int x, y;
	int modo = -1;
	private JToggleButton tbtnStart;
	private JToggleButton tbtnMeta;
	private JToggleButton tbtnDorsal;
	private JToggleButton tbtnBebida;
	private JToggleButton tbtnComida;
	private JToggleButton tbtnSOS;
	private JToggleButton tbtnInterest;
	private JButton btnCambiarmapa;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Mapa frame = new Mapa();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Mapa() {
		setTitle(Messages.getString("Mapa.this.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 732, 546);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 126, 0, 0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0, 0, 0};
		gbl_contentPane.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		{
			scrollPane = new JScrollPane();
			miAreaDibujo = new MiAreaDibujo();
			//miAreaDibujo.addMouseListener(new MiAreaDibujoMouseListener());
			miAreaDibujo.addMouseListener(new MiAreaDibujoMouseListener());//T 19
			miAreaDibujo.setIcon(new ImageIcon(Mapa.class.getResource("/resources/Mapa.png"))); //$NON-NLS-1$
			scrollPane.setViewportView(miAreaDibujo);
			
			toolkit = Toolkit.getDefaultToolkit();
			imagStart = toolkit.getImage(getClass().getClassLoader().getResource("resources/Start.png")); //$NON-NLS-1$
			imagMeta = toolkit.getImage(getClass().getClassLoader().getResource("resources/Finish-Icon-50x50.png")); //$NON-NLS-1$
			imagDorsal = toolkit.getImage(getClass().getClassLoader().getResource("resources/Dorsal.png")); //$NON-NLS-1$
			imagBebida = toolkit.getImage(getClass().getClassLoader().getResource("resources/agua_64.png")); //$NON-NLS-1$
			imagComida = toolkit.getImage(getClass().getClassLoader().getResource("resources/Salad-icon.png")); //$NON-NLS-1$
			imagSOS = toolkit.getImage(getClass().getClassLoader().getResource("resources/SOS_Plain_Red.png")); //$NON-NLS-1$
			imagInteres = toolkit.getImage(getClass().getClassLoader().getResource("resources/interestPoint.png")); //$NON-NLS-1$
			imagen = new ImageIcon(toolkit.getImage(getClass().getClassLoader().getResource("resources/Mapa.png"))); //$NON-NLS-1$
			//cursorStart = imagStart;
			GridBagConstraints gbc_scrollPane = new GridBagConstraints();
			gbc_scrollPane.gridwidth = 3;
			gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
			gbc_scrollPane.fill = GridBagConstraints.BOTH;
			gbc_scrollPane.gridx = 0;
			gbc_scrollPane.gridy = 0;
			contentPane.add(scrollPane, gbc_scrollPane);
		}
		{
			panel = new JPanel();
			panel.setBorder(new TitledBorder(null, Messages.getString("Mapa.0") //$NON-NLS-1$
					+ Messages.getString("Mapa.1"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
			GridBagConstraints gbc_panel = new GridBagConstraints();
			gbc_panel.insets = new Insets(0, 0, 5, 0);
			gbc_panel.fill = GridBagConstraints.BOTH;
			gbc_panel.gridx = 3;
			gbc_panel.gridy = 0;
			contentPane.add(panel, gbc_panel);
			GridBagLayout gbl_panel = new GridBagLayout();
			gbl_panel.columnWidths = new int[]{0, 0, 0};
			gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
			gbl_panel.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
			gbl_panel.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
			panel.setLayout(gbl_panel);
			{
				tbtnStart = new JToggleButton(""); //$NON-NLS-1$
				tbtnStart.addActionListener(new TbtnStartActionListener());
				tbtnStart.setIcon(new ImageIcon(Mapa.class.getResource("/resources/Start.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_tbtnStart = new GridBagConstraints();
				gbc_tbtnStart.insets = new Insets(0, 0, 5, 5);
				gbc_tbtnStart.gridx = 0;
				gbc_tbtnStart.gridy = 0;
				panel.add(tbtnStart, gbc_tbtnStart);
			}
			{
				tbtnMeta = new JToggleButton(""); //$NON-NLS-1$
				tbtnMeta.addActionListener(new TbtnMetaActionListener());
				tbtnMeta.setIcon(new ImageIcon(Mapa.class.getResource("/resources/Finish-Icon-50x50.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_tbtnMeta = new GridBagConstraints();
				gbc_tbtnMeta.insets = new Insets(0, 0, 5, 0);
				gbc_tbtnMeta.gridx = 1;
				gbc_tbtnMeta.gridy = 0;
				panel.add(tbtnMeta, gbc_tbtnMeta);
			}
			{
				tbtnDorsal = new JToggleButton(""); //$NON-NLS-1$
				tbtnDorsal.addActionListener(new TbtnDorsalActionListener());
				tbtnDorsal.setIcon(new ImageIcon(Mapa.class.getResource("/resources/Dorsal.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_tbtnDorsal = new GridBagConstraints();
				gbc_tbtnDorsal.gridwidth = 2;
				gbc_tbtnDorsal.insets = new Insets(0, 0, 5, 5);
				gbc_tbtnDorsal.gridx = 0;
				gbc_tbtnDorsal.gridy = 1;
				panel.add(tbtnDorsal, gbc_tbtnDorsal);
			}
			{
				tbtnBebida = new JToggleButton(""); //$NON-NLS-1$
				tbtnBebida.addActionListener(new TbtnBebidaActionListener());
				tbtnBebida.setIcon(new ImageIcon(Mapa.class.getResource("/resources/agua_64.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_tbtnBebida = new GridBagConstraints();
				gbc_tbtnBebida.insets = new Insets(0, 0, 5, 5);
				gbc_tbtnBebida.gridx = 0;
				gbc_tbtnBebida.gridy = 2;
				panel.add(tbtnBebida, gbc_tbtnBebida);
			}
			{
				tbtnComida = new JToggleButton(""); //$NON-NLS-1$
				tbtnComida.addActionListener(new TbtnComidaActionListener());
				tbtnComida.setIcon(new ImageIcon(Mapa.class.getResource("/resources/Salad-icon.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_tbtnComida = new GridBagConstraints();
				gbc_tbtnComida.insets = new Insets(0, 0, 5, 0);
				gbc_tbtnComida.gridx = 1;
				gbc_tbtnComida.gridy = 2;
				panel.add(tbtnComida, gbc_tbtnComida);
			}
			{
				tbtnSOS = new JToggleButton(""); //$NON-NLS-1$
				tbtnSOS.addActionListener(new TbtnSOSActionListener());
				tbtnSOS.setIcon(new ImageIcon(Mapa.class.getResource("/resources/SOS_Plain_Red.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_tbtnSOS = new GridBagConstraints();
				gbc_tbtnSOS.gridwidth = 2;
				gbc_tbtnSOS.insets = new Insets(0, 0, 5, 5);
				gbc_tbtnSOS.gridx = 0;
				gbc_tbtnSOS.gridy = 3;
				panel.add(tbtnSOS, gbc_tbtnSOS);
			}
			{
				tbtnInterest = new JToggleButton(""); //$NON-NLS-1$
				tbtnInterest.addActionListener(new TbtnInterestActionListener());
				tbtnInterest.setIcon(new ImageIcon(Mapa.class.getResource("/resources/interestPoint.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_tbtnInterest = new GridBagConstraints();
				gbc_tbtnInterest.gridwidth = 2;
				gbc_tbtnInterest.anchor = GridBagConstraints.BELOW_BASELINE;
				gbc_tbtnInterest.insets = new Insets(0, 0, 0, 5);
				gbc_tbtnInterest.gridx = 0;
				gbc_tbtnInterest.gridy = 4;
				panel.add(tbtnInterest, gbc_tbtnInterest);
			}
		}
		{
			btnMostrarPerfil = new JButton(Messages.getString("Mapa.btnMostrarPerfil.text")); //$NON-NLS-1$
			btnMostrarPerfil.addActionListener(new BtnMostrarPerfilActionListener());
			GridBagConstraints gbc_btnMostrarPerfil = new GridBagConstraints();
			gbc_btnMostrarPerfil.insets = new Insets(0, 0, 5, 0);
			gbc_btnMostrarPerfil.gridx = 3;
			gbc_btnMostrarPerfil.gridy = 1;
			contentPane.add(btnMostrarPerfil, gbc_btnMostrarPerfil);
		}
		{
			btnCambiarmapa = new JButton(Messages.getString("Mapa.btnCambiarmapa.text")); //$NON-NLS-1$
			btnCambiarmapa.addActionListener(new BtnCambiarMapaActionListener());
			GridBagConstraints gbc_btnCambiarmapa = new GridBagConstraints();
			gbc_btnCambiarmapa.insets = new Insets(0, 0, 0, 5);
			gbc_btnCambiarmapa.gridx = 0;
			gbc_btnCambiarmapa.gridy = 2;
			contentPane.add(btnCambiarmapa, gbc_btnCambiarmapa);
		}
		{
			btnBorrar = new JButton(Messages.getString("Mapa.btnBorrar.text")); //$NON-NLS-1$
			btnBorrar.addActionListener(new BtnBorrarActionListener());
			GridBagConstraints gbc_btnBorrar = new GridBagConstraints();
			gbc_btnBorrar.insets = new Insets(0, 0, 0, 5);
			gbc_btnBorrar.gridx = 2;
			gbc_btnBorrar.gridy = 2;
			contentPane.add(btnBorrar, gbc_btnBorrar);
		}
	}

	private class BtnMostrarPerfilActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			MapaPerfil ventanaMapa=new MapaPerfil();
			//se hace visible
			ventanaMapa.setVisible(true);
			//se destruye la ventana actual (atributo a nivel de clase)
			dispose();
		}
	}
	private class MiAreaDibujoMouseListener extends MouseAdapter {
		@Override
		public void mousePressed(MouseEvent arg0) {
			//T 20
				x = arg0.getX();
				y = arg0.getY();
				if (imagen != null)
				{
					switch (modo)
					{
					case START:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagStart));
						miAreaDibujo.repaint();
						break;
					case META:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagMeta));
						miAreaDibujo.repaint();
						break;
					case DORSAL:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagDorsal));
						miAreaDibujo.repaint();
						break;
					case BEBIDA:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagBebida));
						miAreaDibujo.repaint();
						break;
					case COMIDA:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagComida));
						miAreaDibujo.repaint();
						break;
					case SOS:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagSOS));
						miAreaDibujo.repaint();
						break;
					case INTERES:
						miAreaDibujo.addObjetoGrafico(new ImagenGrafico(x,y,imagInteres));
						miAreaDibujo.repaint();
						break;
						//T30
					}
				}	
			//T 20
		}
	}			
	private class TbtnStartActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = START;
			tbtnMeta.setSelected(false);
			tbtnBebida.setSelected(false);
			tbtnComida.setSelected(false);
			tbtnDorsal.setSelected(false);
			tbtnSOS.setSelected(false);
			tbtnInterest.setSelected(false);
			if(tbtnStart.isSelected()){
				modo = START;
			}
			else modo = -1;
		}
	}
	private class BtnCambiarMapaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			
			JFileChooser fcAbrir = new JFileChooser();
			int valorDevuelto = fcAbrir.showOpenDialog(frame);
			if (valorDevuelto == JFileChooser.APPROVE_OPTION) {
				File file = fcAbrir.getSelectedFile();
				imagen = new ImageIcon(file.getAbsolutePath());
				miAreaDibujo.clear();
				miAreaDibujo.repaint();
				miAreaDibujo.setIcon(imagen);
			}
	}
}
	private class TbtnMetaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			tbtnStart.setSelected(false);
			tbtnBebida.setSelected(false);
			tbtnComida.setSelected(false);
			tbtnDorsal.setSelected(false);
			tbtnSOS.setSelected(false);
			tbtnInterest.setSelected(false);
			if(tbtnMeta.isSelected()){
				modo = META;
			}
			else modo = -1;
		}
	}
	private class TbtnDorsalActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = DORSAL;
			tbtnStart.setSelected(false);
			tbtnMeta.setSelected(false);
			tbtnBebida.setSelected(false);
			tbtnComida.setSelected(false);
			tbtnSOS.setSelected(false);
			tbtnInterest.setSelected(false);
			if(tbtnDorsal.isSelected()){
				modo = DORSAL;
			}
			else modo = -1;
		}
	}
	private class TbtnBebidaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = BEBIDA;
			tbtnStart.setSelected(false);
			tbtnMeta.setSelected(false);
			tbtnComida.setSelected(false);
			tbtnDorsal.setSelected(false);
			tbtnSOS.setSelected(false);
			tbtnInterest.setSelected(false);
			if(tbtnBebida.isSelected()){
				modo = BEBIDA;
			}
			else modo = -1;
		}
	}
	private class TbtnComidaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = COMIDA;
			tbtnStart.setSelected(false);
			tbtnMeta.setSelected(false);
			tbtnBebida.setSelected(false);
			tbtnDorsal.setSelected(false);
			tbtnSOS.setSelected(false);
			tbtnInterest.setSelected(false);
			if(tbtnComida.isSelected()){
				modo = COMIDA;
			}
			else modo = -1;
		}
	}
	private class TbtnSOSActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = SOS;
			tbtnStart.setSelected(false);
			tbtnMeta.setSelected(false);
			tbtnBebida.setSelected(false);
			tbtnComida.setSelected(false);
			tbtnDorsal.setSelected(false);
			tbtnInterest.setSelected(false);
			if(tbtnSOS.isSelected()){
				modo = SOS;
			}
			else modo = -1;
		}
	}
	private class TbtnInterestActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			modo = INTERES;
			tbtnStart.setSelected(false);
			tbtnMeta.setSelected(false);
			tbtnBebida.setSelected(false);
			tbtnComida.setSelected(false);
			tbtnDorsal.setSelected(false);
			tbtnSOS.setSelected(false);
			if(tbtnInterest.isSelected()){
				modo = INTERES;
			}
			else modo = -1;
		}
	}
	private class BtnBorrarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			miAreaDibujo.clear();
			miAreaDibujo.repaint();
		}
	}
}
