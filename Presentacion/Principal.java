package Presentacion;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.MaskFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.ParseException;

public class Principal extends JFrame {

	private JPanel contentPane;
	private JPanel panelBarra;
	private JPanel panelPrincipal;
	private JLabel lblCompeticiones;
	private JTabbedPane tabbedPane;
	private JPanel pDatos;
	private JPanel pOrganizaciones;
	private JPanel pCartel;
	private JPanel pResultados;
	private JPanel pAdicional;
	private JPanel panel;
	private JPanel panel_1;
	private JPanel panel_2;
	private JPanel panel_3;
	private JPanel panel_4;
	private JButton btnParticipantes;
	private JLabel lblLocalidad;
	private JLabel lblFecha;
	private JLabel lblHora;
	private JLabel lblModalidad;
	private JLabel lblDificultad;
	private JLabel lblDistancia;
	private JLabel lblInscripcin;
	private JLabel lblPlazas;
	private JLabel lblFechaLimiteDe;
	private JLabel lblPremio;
	private JLabel lblPremio2;
	private JLabel lblPremio3;
	private JTextField txtPremio1;
	private JTextField txtPremio2;
	private JTextField txtPremio3;
	private JFormattedTextField ftxtHora;
	private JComboBox cbDificultad;
	private JComboBox cbModalidad;
	private JTextField txtDistancia;
	private JTextPane txtpnDescripcin;
	private JLabel lblIngreso;
	private JLabel lblFechaIngreso;
	private JLabel lblNombre;
	private JLabel lblRol;
	private JLabel lblNombreUsuario;
	private JButton btnCerrarSesin;
	private JButton btnNuevo;
	private JToggleButton btnModificar;
	private JButton btnBorrar;
	private JPanel panelCompeticiones;
	private JLabel lblOrganizadores;
	private JLabel lblColaboradores;
	private JLabel lblCartel;
	private JPanel panel_5;
	private JPanel panel_6;
	private JScrollPane scrollPane_1;
	private JLabel lblTiempo;
	private JFormattedTextField frmtdtxtfldTiempo;
	private JLabel lblNParticipantes;
	private JLabel lblNAbandonos;
	private JPanel panel_7;
	private JPanel panel_8;
	private JPanel panel_9;
	private JPanel panel_10;
	private JLabel lblCmoLlegar;
	private JLabel lblConsejos;
	private JTextArea txtrConvieneDesayunarFuerte;
	private JTextArea txtrComoLlegar;
	private JLabel lblPrimero;
	private JLabel lblSegundo;
	private JLabel lblTercero;
	private JTextField txtPrimero;
	private JTextField txtSegundo;
	private JTextField txtTercero;
	private JScrollPane scrollPane;
	private JList lstCompeticiones;
	private JScrollPane scrollPane_2;
	private JList list_1;
	private JList list_2;
	private JButton btnMapa;
	private JTextField txtLocalidad;
	private JTextField textEmail;
	private JTextField textWeb;
	private JLabel lblEmail;
	private JLabel lblTelefono;
	private JLabel lblWeb;
	private JFormattedTextField ftxtNParticipantes;
	private JFormattedTextField ftxtNAbandonos;
	private JFormattedTextField ftxtCoste;
	private JFormattedTextField ftxtCoste_1;
	private JFormattedTextField ftxtTelefono;
	private JFormattedTextField ftxtPlazas;
	private JComboBox cbDiaInscr;
	private JComboBox cbMesInscr;
	private DefaultListModel modeloListaComp;
	private JComboBox cbAnioInscr;
	
	private int seleccion;
	private JComboBox cbDia;
	private JComboBox cbMes;
	private JComboBox cbAnio;
	
	private JFrame frame;
	private JCheckBox chckFinalizada;
	private JMenuBar menuBar;
	private JMenu mnAyuda;
	private JMenuItem mntmAcercaDe;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Principal frame = new Principal("Sergio95"); //$NON-NLS-1$
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Principal(String usuario) {
		setTitle(Messages.getString("Principal.this.title")); //$NON-NLS-1$
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 925, 517);
		{
			menuBar = new JMenuBar();
			setJMenuBar(menuBar);
			{
				mnAyuda = new JMenu(Messages.getString("Principal.mnAyuda.text")); //$NON-NLS-1$
				menuBar.add(mnAyuda);
				{
					mntmAcercaDe = new JMenuItem(Messages.getString("Principal.mntmAcercaDe.text")); //$NON-NLS-1$
					mntmAcercaDe.addActionListener(new MntmAcercaDeActionListener());
					mnAyuda.add(mntmAcercaDe);
				}
			}
		}
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		{
			panelBarra = new JPanel();
			contentPane.add(panelBarra, BorderLayout.NORTH);
			GridBagLayout gbl_panelBarra = new GridBagLayout();
			gbl_panelBarra.columnWidths = new int[]{71, 102, 180, 75, 74, 140, 0};
			gbl_panelBarra.rowHeights = new int[]{23, 0};
			gbl_panelBarra.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
			gbl_panelBarra.rowWeights = new double[]{0.0, Double.MIN_VALUE};
			panelBarra.setLayout(gbl_panelBarra);
			{
				lblIngreso = new JLabel(Messages.getString("Principal.lblIngreso.text")); //$NON-NLS-1$
				GridBagConstraints gbc_lblIngreso = new GridBagConstraints();
				gbc_lblIngreso.anchor = GridBagConstraints.WEST;
				gbc_lblIngreso.insets = new Insets(0, 0, 0, 5);
				gbc_lblIngreso.gridx = 0;
				gbc_lblIngreso.gridy = 0;
				panelBarra.add(lblIngreso, gbc_lblIngreso);
			}
			{
				lblFechaIngreso = new JLabel("2014-08-12"); //$NON-NLS-1$
				GridBagConstraints gbc_lblFechaIngreso = new GridBagConstraints();
				gbc_lblFechaIngreso.anchor = GridBagConstraints.WEST;
				gbc_lblFechaIngreso.insets = new Insets(0, 0, 0, 5);
				gbc_lblFechaIngreso.gridx = 1;
				gbc_lblFechaIngreso.gridy = 0;
				panelBarra.add(lblFechaIngreso, gbc_lblFechaIngreso);
			}
			{
				lblNombre = new JLabel("Manuel Salinas Pérez"); //$NON-NLS-1$
				GridBagConstraints gbc_lblNombre = new GridBagConstraints();
				gbc_lblNombre.anchor = GridBagConstraints.WEST;
				gbc_lblNombre.insets = new Insets(0, 0, 0, 5);
				gbc_lblNombre.gridx = 2;
				gbc_lblNombre.gridy = 0;
				panelBarra.add(lblNombre, gbc_lblNombre);
			}
			{
				lblRol = new JLabel(Messages.getString("Principal.lblRol.text")); //$NON-NLS-1$
				GridBagConstraints gbc_lblRol = new GridBagConstraints();
				gbc_lblRol.anchor = GridBagConstraints.WEST;
				gbc_lblRol.insets = new Insets(0, 0, 0, 5);
				gbc_lblRol.gridx = 3;
				gbc_lblRol.gridy = 0;
				panelBarra.add(lblRol, gbc_lblRol);
			}
			{
				lblNombreUsuario = new JLabel(usuario);
				GridBagConstraints gbc_lblNombreUsuario = new GridBagConstraints();
				gbc_lblNombreUsuario.anchor = GridBagConstraints.WEST;
				gbc_lblNombreUsuario.insets = new Insets(0, 0, 0, 5);
				gbc_lblNombreUsuario.gridx = 4;
				gbc_lblNombreUsuario.gridy = 0;
				panelBarra.add(lblNombreUsuario, gbc_lblNombreUsuario);
			}
			{
				btnCerrarSesin = new JButton(Messages.getString("Principal.btnCerrarSesin.text")); //$NON-NLS-1$
				btnCerrarSesin.addActionListener(new BtnCerrarSesinActionListener());
				GridBagConstraints gbc_btnCerrarSesin = new GridBagConstraints();
				gbc_btnCerrarSesin.anchor = GridBagConstraints.NORTHEAST;
				gbc_btnCerrarSesin.gridx = 5;
				gbc_btnCerrarSesin.gridy = 0;
				panelBarra.add(btnCerrarSesin, gbc_btnCerrarSesin);
			}
		}
		{
			panelPrincipal = new JPanel();
			contentPane.add(panelPrincipal, BorderLayout.CENTER);
			GridBagLayout gbl_panelPrincipal = new GridBagLayout();
			gbl_panelPrincipal.columnWidths = new int[]{0, 0, 0, 0};
			gbl_panelPrincipal.rowHeights = new int[]{0, 0, 0, 44, 0};
			gbl_panelPrincipal.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
			gbl_panelPrincipal.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
			panelPrincipal.setLayout(gbl_panelPrincipal);
			{
				tabbedPane = new JTabbedPane(JTabbedPane.TOP);
				GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
				gbc_tabbedPane.gridheight = 4;
				gbc_tabbedPane.fill = GridBagConstraints.BOTH;
				gbc_tabbedPane.gridx = 2;
				gbc_tabbedPane.gridy = 0;
				panelPrincipal.add(tabbedPane, gbc_tabbedPane);
				{
					pDatos = new JPanel();
					tabbedPane.addTab(Messages.getString("Principal.Data"), null, pDatos, null); //$NON-NLS-1$
					GridBagLayout gbl_pDatos = new GridBagLayout();
					gbl_pDatos.columnWidths = new int[]{276, 0, 0};
					gbl_pDatos.rowHeights = new int[]{0, 89, 0, 50, 0, 101, 0};
					gbl_pDatos.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
					gbl_pDatos.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
					pDatos.setLayout(gbl_pDatos);
					{
						panel = new JPanel();
						panel.setBorder(new TitledBorder(null, Messages.getString("Principal.panel.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
						GridBagConstraints gbc_panel = new GridBagConstraints();
						gbc_panel.gridheight = 3;
						gbc_panel.insets = new Insets(0, 0, 5, 5);
						gbc_panel.fill = GridBagConstraints.BOTH;
						gbc_panel.gridx = 0;
						gbc_panel.gridy = 1;
						pDatos.add(panel, gbc_panel);
						GridBagLayout gbl_panel = new GridBagLayout();
						gbl_panel.columnWidths = new int[]{77, 56, 0, 0, 0};
						gbl_panel.rowHeights = new int[]{24, 24, 24, 24, 24, 24, 0, 0};
						gbl_panel.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
						gbl_panel.rowWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
						panel.setLayout(gbl_panel);
						{
							chckFinalizada = new JCheckBox(Messages.getString(Messages.getString("Principal.62"))); //$NON-NLS-1$
							chckFinalizada.setEnabled(false);
							GridBagConstraints gbc_chckFinalizada = new GridBagConstraints();
							gbc_chckFinalizada.gridwidth = 4;
							gbc_chckFinalizada.insets = new Insets(0, 0, 5, 5);
							gbc_chckFinalizada.gridx = 0;
							gbc_chckFinalizada.gridy = 0;
							panel.add(chckFinalizada, gbc_chckFinalizada);
						}

						{
							lblLocalidad = new JLabel(Messages.getString("Principal.lblLocalidad.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblLocalidad = new GridBagConstraints();
							gbc_lblLocalidad.anchor = GridBagConstraints.WEST;
							gbc_lblLocalidad.fill = GridBagConstraints.VERTICAL;
							gbc_lblLocalidad.insets = new Insets(0, 0, 5, 5);
							gbc_lblLocalidad.gridx = 0;
							gbc_lblLocalidad.gridy = 1;
							panel.add(lblLocalidad, gbc_lblLocalidad);
						}
						{
							txtLocalidad = new JTextField();
							txtLocalidad.setEditable(false);
							txtLocalidad.setText(Messages.getString("Principal.txtLocalidad.text")); //$NON-NLS-1$
							GridBagConstraints gbc_txtLocalidad = new GridBagConstraints();
							gbc_txtLocalidad.gridwidth = 3;
							gbc_txtLocalidad.insets = new Insets(0, 0, 5, 0);
							gbc_txtLocalidad.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtLocalidad.gridx = 1;
							gbc_txtLocalidad.gridy = 1;
							panel.add(txtLocalidad, gbc_txtLocalidad);
							txtLocalidad.setColumns(10);
						}
						{
							lblFecha = new JLabel(Messages.getString("Principal.lblFecha.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblFecha = new GridBagConstraints();
							gbc_lblFecha.anchor = GridBagConstraints.WEST;
							gbc_lblFecha.fill = GridBagConstraints.VERTICAL;
							gbc_lblFecha.insets = new Insets(0, 0, 5, 5);
							gbc_lblFecha.gridx = 0;
							gbc_lblFecha.gridy = 2;
							panel.add(lblFecha, gbc_lblFecha);
						}
						{
							cbDia = new JComboBox();
							cbDia.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$
							cbDia.setSelectedIndex(3);
							cbDia.addFocusListener(new MiComboBoxFocusListener());
							cbDia.addActionListener(new MiComboBoxActionListener());
							GridBagConstraints gbc_cbDia = new GridBagConstraints();
							gbc_cbDia.insets = new Insets(0, 0, 5, 5);
							gbc_cbDia.fill = GridBagConstraints.HORIZONTAL;
							gbc_cbDia.gridx = 1;
							gbc_cbDia.gridy = 2;
							panel.add(cbDia, gbc_cbDia);
						}
						{
							cbMes = new JComboBox();
							cbMes.setModel(new DefaultComboBoxModel(new String[] {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$
							cbMes.setSelectedIndex(2);
							cbMes.addFocusListener(new MiComboBoxFocusListener());
							cbMes.addActionListener(new MiComboBoxActionListener());
							GridBagConstraints gbc_cbMes = new GridBagConstraints();
							gbc_cbMes.insets = new Insets(0, 0, 5, 5);
							gbc_cbMes.fill = GridBagConstraints.HORIZONTAL;
							gbc_cbMes.gridx = 2;
							gbc_cbMes.gridy = 2;
							panel.add(cbMes, gbc_cbMes);
						}
						{
							cbAnio = new JComboBox();
							cbAnio.setModel(new DefaultComboBoxModel(new String[] {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$ //$NON-NLS-32$ //$NON-NLS-33$ //$NON-NLS-34$ //$NON-NLS-35$ //$NON-NLS-36$ //$NON-NLS-37$ //$NON-NLS-38$ //$NON-NLS-39$ //$NON-NLS-40$ //$NON-NLS-41$ //$NON-NLS-42$ //$NON-NLS-43$ //$NON-NLS-44$ //$NON-NLS-45$ //$NON-NLS-46$ //$NON-NLS-47$ //$NON-NLS-48$ //$NON-NLS-49$ //$NON-NLS-50$ //$NON-NLS-51$ //$NON-NLS-52$ //$NON-NLS-53$ //$NON-NLS-54$ //$NON-NLS-55$ //$NON-NLS-56$ //$NON-NLS-57$ //$NON-NLS-58$ //$NON-NLS-59$ //$NON-NLS-60$ //$NON-NLS-61$ //$NON-NLS-62$ //$NON-NLS-63$ //$NON-NLS-64$ //$NON-NLS-65$ //$NON-NLS-66$ //$NON-NLS-67$ //$NON-NLS-68$ //$NON-NLS-69$ //$NON-NLS-70$ //$NON-NLS-71$ //$NON-NLS-72$ //$NON-NLS-73$ //$NON-NLS-74$ //$NON-NLS-75$ //$NON-NLS-76$ //$NON-NLS-77$ //$NON-NLS-78$ //$NON-NLS-79$ //$NON-NLS-80$ //$NON-NLS-81$ //$NON-NLS-82$ //$NON-NLS-83$ //$NON-NLS-84$ //$NON-NLS-85$ //$NON-NLS-86$ //$NON-NLS-87$ //$NON-NLS-88$ //$NON-NLS-89$ //$NON-NLS-90$ //$NON-NLS-91$ //$NON-NLS-92$ //$NON-NLS-93$ //$NON-NLS-94$ //$NON-NLS-95$ //$NON-NLS-96$ //$NON-NLS-97$ //$NON-NLS-98$ //$NON-NLS-99$ //$NON-NLS-100$
							cbAnio.setSelectedIndex(16);
							cbAnio.addFocusListener(new MiComboBoxFocusListener());
							cbAnio.addActionListener(new MiComboBoxActionListener());
							GridBagConstraints gbc_cbAnio = new GridBagConstraints();
							gbc_cbAnio.insets = new Insets(0, 0, 5, 0);
							gbc_cbAnio.fill = GridBagConstraints.HORIZONTAL;
							gbc_cbAnio.gridx = 3;
							gbc_cbAnio.gridy = 2;
							panel.add(cbAnio, gbc_cbAnio);
						}
						{
							lblHora = new JLabel(Messages.getString("Principal.lblHora.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblHora = new GridBagConstraints();
							gbc_lblHora.anchor = GridBagConstraints.WEST;
							gbc_lblHora.fill = GridBagConstraints.VERTICAL;
							gbc_lblHora.insets = new Insets(0, 0, 5, 5);
							gbc_lblHora.gridx = 0;
							gbc_lblHora.gridy = 3;
							panel.add(lblHora, gbc_lblHora);
						}
						
						MaskFormatter formatoHora;
						try {
							formatoHora = new MaskFormatter("##:##"); //$NON-NLS-1$
							formatoHora.setPlaceholderCharacter('_');
							ftxtHora = new JFormattedTextField(formatoHora);
							ftxtHora.addFocusListener(new FtxtHoraFocusListener());
						}catch (ParseException e){
							e.printStackTrace();
						}
				        
						ftxtHora.setEditable(false);
						ftxtHora.setText("08:30"); //$NON-NLS-1$
						GridBagConstraints gbc_ftxtHora = new GridBagConstraints();
						gbc_ftxtHora.gridwidth = 3;
						gbc_ftxtHora.insets = new Insets(0, 0, 5, 0);
						gbc_ftxtHora.fill = GridBagConstraints.HORIZONTAL;
						gbc_ftxtHora.gridx = 1;
						gbc_ftxtHora.gridy = 3;
						panel.add(ftxtHora, gbc_ftxtHora);
						{
							lblModalidad = new JLabel(Messages.getString("Principal.lblModalidad.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblModalidad = new GridBagConstraints();
							gbc_lblModalidad.anchor = GridBagConstraints.WEST;
							gbc_lblModalidad.fill = GridBagConstraints.VERTICAL;
							gbc_lblModalidad.insets = new Insets(0, 0, 5, 5);
							gbc_lblModalidad.gridx = 0;
							gbc_lblModalidad.gridy = 4;
							panel.add(lblModalidad, gbc_lblModalidad);
						}
						{
							cbModalidad = new JComboBox();
							cbModalidad.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("Principal.0"), Messages.getString("Principal.1"), Messages.getString("Principal.2"), Messages.getString("Principal.3")})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
							cbModalidad.setSelectedIndex(0);
							cbModalidad.addFocusListener(new MiComboBoxFocusListener());
							cbModalidad.addActionListener(new MiComboBoxActionListener());
							GridBagConstraints gbc_cbModalidad = new GridBagConstraints();
							gbc_cbModalidad.gridwidth = 3;
							gbc_cbModalidad.fill = GridBagConstraints.HORIZONTAL;
							gbc_cbModalidad.anchor = GridBagConstraints.SOUTH;
							gbc_cbModalidad.insets = new Insets(0, 0, 5, 0);
							gbc_cbModalidad.gridx = 1;
							gbc_cbModalidad.gridy = 4;
							panel.add(cbModalidad, gbc_cbModalidad);
						}
						{
							lblDificultad = new JLabel(Messages.getString("Principal.lblDificultad.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblDificultad = new GridBagConstraints();
							gbc_lblDificultad.anchor = GridBagConstraints.WEST;
							gbc_lblDificultad.fill = GridBagConstraints.VERTICAL;
							gbc_lblDificultad.insets = new Insets(0, 0, 5, 5);
							gbc_lblDificultad.gridx = 0;
							gbc_lblDificultad.gridy = 5;
							panel.add(lblDificultad, gbc_lblDificultad);
						}
						{
							cbDificultad = new JComboBox();
							cbDificultad.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("Principal.4"), Messages.getString("Principal.5"), Messages.getString("Principal.6")})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
							cbDificultad.setSelectedIndex(0);
							cbDificultad.addFocusListener(new MiComboBoxFocusListener());
							cbDificultad.addActionListener(new MiComboBoxActionListener());
							GridBagConstraints gbc_cbDificultad = new GridBagConstraints();
							gbc_cbDificultad.gridwidth = 3;
							gbc_cbDificultad.insets = new Insets(0, 0, 5, 0);
							gbc_cbDificultad.fill = GridBagConstraints.HORIZONTAL;
							gbc_cbDificultad.gridx = 1;
							gbc_cbDificultad.gridy = 5;
							panel.add(cbDificultad, gbc_cbDificultad);
						}
						{
							lblDistancia = new JLabel(Messages.getString("Principal.lblDistancia.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblDistancia = new GridBagConstraints();
							gbc_lblDistancia.anchor = GridBagConstraints.WEST;
							gbc_lblDistancia.insets = new Insets(0, 0, 0, 5);
							gbc_lblDistancia.fill = GridBagConstraints.VERTICAL;
							gbc_lblDistancia.gridx = 0;
							gbc_lblDistancia.gridy = 6;
							panel.add(lblDistancia, gbc_lblDistancia);
						}
						{
							txtDistancia = new JTextField();
							txtDistancia.setEditable(false);
							txtDistancia.setText("6 km"); //$NON-NLS-1$
							GridBagConstraints gbc_txtDistancia = new GridBagConstraints();
							gbc_txtDistancia.gridwidth = 3;
							gbc_txtDistancia.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtDistancia.gridx = 1;
							gbc_txtDistancia.gridy = 6;
							panel.add(txtDistancia, gbc_txtDistancia);
							txtDistancia.setColumns(10);
						}
					}
					{
						panel_1 = new JPanel();
						panel_1.setBorder(new TitledBorder(null, Messages.getString("Principal.panel_1.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
						GridBagConstraints gbc_panel_1 = new GridBagConstraints();
						gbc_panel_1.gridheight = 2;
						gbc_panel_1.insets = new Insets(0, 0, 5, 0);
						gbc_panel_1.fill = GridBagConstraints.BOTH;
						gbc_panel_1.gridx = 1;
						gbc_panel_1.gridy = 0;
						pDatos.add(panel_1, gbc_panel_1);
						GridBagLayout gbl_panel_1 = new GridBagLayout();
						gbl_panel_1.columnWidths = new int[]{62, 73, 0, 0};
						gbl_panel_1.rowHeights = new int[]{27, 0, 0, 0, 0};
						gbl_panel_1.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0};
						gbl_panel_1.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
						panel_1.setLayout(gbl_panel_1);
						{
							lblInscripcin = new JLabel(Messages.getString("Principal.lblInscripcin.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblInscripcin = new GridBagConstraints();
							gbc_lblInscripcin.gridwidth = 2;
							gbc_lblInscripcin.anchor = GridBagConstraints.WEST;
							gbc_lblInscripcin.insets = new Insets(0, 0, 5, 5);
							gbc_lblInscripcin.gridx = 0;
							gbc_lblInscripcin.gridy = 0;
							panel_1.add(lblInscripcin, gbc_lblInscripcin);
						}
						{
							MaskFormatter formatoCoste;
							try {
								formatoCoste = new MaskFormatter("####' €"); //$NON-NLS-1$
								formatoCoste.setPlaceholderCharacter(' ');
								ftxtCoste_1 = new JFormattedTextField(formatoCoste);
								ftxtCoste_1.setEditable(false);
							}catch (ParseException e){
								e.printStackTrace();
							}
							
							ftxtCoste_1.setText("15"); //$NON-NLS-1$

							
							GridBagConstraints gbc_ftxtCoste = new GridBagConstraints();
							gbc_ftxtCoste.gridwidth = 2;
							gbc_ftxtCoste.insets = new Insets(0, 0, 5, 0);
							gbc_ftxtCoste.fill = GridBagConstraints.HORIZONTAL;
							gbc_ftxtCoste.gridx = 2;
							gbc_ftxtCoste.gridy = 0;
							panel_1.add(ftxtCoste_1, gbc_ftxtCoste);
						}
						{
							lblPlazas = new JLabel(Messages.getString("Principal.lblPlazas.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblPlazas = new GridBagConstraints();
							gbc_lblPlazas.gridwidth = 2;
							gbc_lblPlazas.anchor = GridBagConstraints.WEST;
							gbc_lblPlazas.insets = new Insets(0, 0, 5, 5);
							gbc_lblPlazas.gridx = 0;
							gbc_lblPlazas.gridy = 1;
							panel_1.add(lblPlazas, gbc_lblPlazas);
						}
						{
							MaskFormatter formatoPlazas;
							try {
								formatoPlazas = new MaskFormatter("####'"); //$NON-NLS-1$
								formatoPlazas.setPlaceholderCharacter(' ');
								ftxtPlazas = new JFormattedTextField(formatoPlazas);
								ftxtPlazas.setEditable(false);
							}catch (ParseException e){
								e.printStackTrace();
							}
							ftxtPlazas.setText("90"); //$NON-NLS-1$
							
							GridBagConstraints gbc_ftxtPlazas = new GridBagConstraints();
							gbc_ftxtPlazas.gridwidth = 2;
							gbc_ftxtPlazas.insets = new Insets(0, 0, 5, 0);
							gbc_ftxtPlazas.fill = GridBagConstraints.HORIZONTAL;
							gbc_ftxtPlazas.gridx = 2;
							gbc_ftxtPlazas.gridy = 1;
							panel_1.add(ftxtPlazas, gbc_ftxtPlazas);
						}
						{
							lblFechaLimiteDe = new JLabel(Messages.getString("Principal.lblFechaLimiteDe.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblFechaLimiteDe = new GridBagConstraints();
							gbc_lblFechaLimiteDe.gridwidth = 4;
							gbc_lblFechaLimiteDe.insets = new Insets(0, 0, 5, 0);
							gbc_lblFechaLimiteDe.gridx = 0;
							gbc_lblFechaLimiteDe.gridy = 2;
							panel_1.add(lblFechaLimiteDe, gbc_lblFechaLimiteDe);
						}
						{
							cbDiaInscr = new JComboBox();
							cbDiaInscr.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$
							cbDiaInscr.setSelectedIndex(3);
							cbDiaInscr.addFocusListener(new MiComboBoxFocusListener());
							cbDiaInscr.addActionListener(new MiComboBoxActionListener());
							GridBagConstraints gbc_cbDiaInscr = new GridBagConstraints();
							gbc_cbDiaInscr.insets = new Insets(0, 0, 0, 5);
							gbc_cbDiaInscr.fill = GridBagConstraints.HORIZONTAL;
							gbc_cbDiaInscr.gridx = 0;
							gbc_cbDiaInscr.gridy = 3;
							panel_1.add(cbDiaInscr, gbc_cbDiaInscr);
						}
						{
							cbMesInscr = new JComboBox();
							cbMesInscr.setModel(new DefaultComboBoxModel(new String[] {Messages.getString("Principal.7"), Messages.getString("Principal.8"), Messages.getString("Principal.9"), Messages.getString("Principal.10"), Messages.getString("Principal.11"), Messages.getString("Principal.12"), Messages.getString("Principal.13"), Messages.getString("Principal.14"), Messages.getString("Principal.15"), Messages.getString("Principal.16"), Messages.getString("Principal.17"), Messages.getString("Principal.18")})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$
							cbMesInscr.setSelectedIndex(5);
							cbMesInscr.addFocusListener(new MiComboBoxFocusListener());
							cbMesInscr.addActionListener(new MiComboBoxActionListener());
							GridBagConstraints gbc_cbMesInscr = new GridBagConstraints();
							gbc_cbMesInscr.fill = GridBagConstraints.HORIZONTAL;
							gbc_cbMesInscr.insets = new Insets(0, 0, 0, 5);
							gbc_cbMesInscr.gridx = 1;
							gbc_cbMesInscr.gridy = 3;
							panel_1.add(cbMesInscr, gbc_cbMesInscr);
						}
						{
							cbAnioInscr = new JComboBox();
							cbAnioInscr.setModel(new DefaultComboBoxModel(new String[] {"2000", "2001", "2002", "2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032", "2033", "2034", "2035", "2036", "2037", "2038", "2039", "2040", "2041", "2042", "2043", "2044", "2045", "2046", "2047", "2048", "2049", "2050", "2051", "2052", "2053", "2054", "2055", "2056", "2057", "2058", "2059", "2060", "2061", "2062", "2063", "2064", "2065", "2066", "2067", "2068", "2069", "2070", "2071", "2072", "2073", "2074", "2075", "2076", "2077", "2078", "2079", "2080", "2081", "2082", "2083", "2084", "2085", "2086", "2087", "2088", "2089", "2090", "2091", "2092", "2093", "2094", "2095", "2096", "2097", "2098", "2099"})); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$ //$NON-NLS-8$ //$NON-NLS-9$ //$NON-NLS-10$ //$NON-NLS-11$ //$NON-NLS-12$ //$NON-NLS-13$ //$NON-NLS-14$ //$NON-NLS-15$ //$NON-NLS-16$ //$NON-NLS-17$ //$NON-NLS-18$ //$NON-NLS-19$ //$NON-NLS-20$ //$NON-NLS-21$ //$NON-NLS-22$ //$NON-NLS-23$ //$NON-NLS-24$ //$NON-NLS-25$ //$NON-NLS-26$ //$NON-NLS-27$ //$NON-NLS-28$ //$NON-NLS-29$ //$NON-NLS-30$ //$NON-NLS-31$ //$NON-NLS-32$ //$NON-NLS-33$ //$NON-NLS-34$ //$NON-NLS-35$ //$NON-NLS-36$ //$NON-NLS-37$ //$NON-NLS-38$ //$NON-NLS-39$ //$NON-NLS-40$ //$NON-NLS-41$ //$NON-NLS-42$ //$NON-NLS-43$ //$NON-NLS-44$ //$NON-NLS-45$ //$NON-NLS-46$ //$NON-NLS-47$ //$NON-NLS-48$ //$NON-NLS-49$ //$NON-NLS-50$ //$NON-NLS-51$ //$NON-NLS-52$ //$NON-NLS-53$ //$NON-NLS-54$ //$NON-NLS-55$ //$NON-NLS-56$ //$NON-NLS-57$ //$NON-NLS-58$ //$NON-NLS-59$ //$NON-NLS-60$ //$NON-NLS-61$ //$NON-NLS-62$ //$NON-NLS-63$ //$NON-NLS-64$ //$NON-NLS-65$ //$NON-NLS-66$ //$NON-NLS-67$ //$NON-NLS-68$ //$NON-NLS-69$ //$NON-NLS-70$ //$NON-NLS-71$ //$NON-NLS-72$ //$NON-NLS-73$ //$NON-NLS-74$ //$NON-NLS-75$ //$NON-NLS-76$ //$NON-NLS-77$ //$NON-NLS-78$ //$NON-NLS-79$ //$NON-NLS-80$ //$NON-NLS-81$ //$NON-NLS-82$ //$NON-NLS-83$ //$NON-NLS-84$ //$NON-NLS-85$ //$NON-NLS-86$ //$NON-NLS-87$ //$NON-NLS-88$ //$NON-NLS-89$ //$NON-NLS-90$ //$NON-NLS-91$ //$NON-NLS-92$ //$NON-NLS-93$ //$NON-NLS-94$ //$NON-NLS-95$ //$NON-NLS-96$ //$NON-NLS-97$ //$NON-NLS-98$ //$NON-NLS-99$ //$NON-NLS-100$
							cbAnioInscr.setSelectedIndex(15);
							cbAnioInscr.addFocusListener(new MiComboBoxFocusListener());
							cbAnioInscr.addActionListener(new MiComboBoxActionListener());
							GridBagConstraints gbc_cbAnioInscr = new GridBagConstraints();
							gbc_cbAnioInscr.gridwidth = 2;
							gbc_cbAnioInscr.insets = new Insets(0, 0, 0, 5);
							gbc_cbAnioInscr.fill = GridBagConstraints.HORIZONTAL;
							gbc_cbAnioInscr.gridx = 2;
							gbc_cbAnioInscr.gridy = 3;
							panel_1.add(cbAnioInscr, gbc_cbAnioInscr);
						}
					}
					{
						panel_2 = new JPanel();
						panel_2.setBorder(new TitledBorder(null, Messages.getString("Principal.panel_2.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
						GridBagConstraints gbc_panel_2 = new GridBagConstraints();
						gbc_panel_2.gridheight = 2;
						gbc_panel_2.insets = new Insets(0, 0, 5, 0);
						gbc_panel_2.fill = GridBagConstraints.BOTH;
						gbc_panel_2.gridx = 1;
						gbc_panel_2.gridy = 2;
						pDatos.add(panel_2, gbc_panel_2);
						GridBagLayout gbl_panel_2 = new GridBagLayout();
						gbl_panel_2.columnWidths = new int[]{0, 25, 0};
						gbl_panel_2.rowHeights = new int[]{0, 0, 0, 13, 0};
						gbl_panel_2.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
						gbl_panel_2.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
						panel_2.setLayout(gbl_panel_2);
						{
							textEmail = new JTextField();
							textEmail.setText("carreras@veloces.com"); //$NON-NLS-1$
							textEmail.setEditable(false);
							GridBagConstraints gbc_textEmail = new GridBagConstraints();
							gbc_textEmail.gridheight = 2;
							gbc_textEmail.insets = new Insets(0, 0, 5, 0);
							gbc_textEmail.fill = GridBagConstraints.HORIZONTAL;
							gbc_textEmail.gridx = 1;
							gbc_textEmail.gridy = 0;
							panel_2.add(textEmail, gbc_textEmail);
							textEmail.setColumns(10);
						}
						{
							lblEmail = new JLabel(Messages.getString("Principal.lblEmail.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblEmail = new GridBagConstraints();
							gbc_lblEmail.anchor = GridBagConstraints.WEST;
							gbc_lblEmail.insets = new Insets(0, 0, 5, 5);
							gbc_lblEmail.gridx = 0;
							gbc_lblEmail.gridy = 1;
							panel_2.add(lblEmail, gbc_lblEmail);
						}
						{
							lblTelefono = new JLabel(Messages.getString("Principal.lblTelefono.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblTelefono = new GridBagConstraints();
							gbc_lblTelefono.insets = new Insets(0, 0, 5, 5);
							gbc_lblTelefono.anchor = GridBagConstraints.EAST;
							gbc_lblTelefono.gridx = 0;
							gbc_lblTelefono.gridy = 2;
							panel_2.add(lblTelefono, gbc_lblTelefono);
						}
						
						{						
							MaskFormatter formatoTlfno;
							try {
								formatoTlfno = new MaskFormatter("'(###')' ###' ###' ###"); //$NON-NLS-1$
								formatoTlfno.setPlaceholderCharacter('*');
								ftxtTelefono = new JFormattedTextField(formatoTlfno);
							}catch (ParseException e){
								e.printStackTrace();
							}
							ftxtTelefono.setText("034 612 345 678"); //$NON-NLS-1$
							ftxtTelefono.setEditable(false);
							
							GridBagConstraints gbc_ftxtTelefono = new GridBagConstraints();
							gbc_ftxtTelefono.insets = new Insets(0, 0, 5, 0);
							gbc_ftxtTelefono.fill = GridBagConstraints.HORIZONTAL;
							gbc_ftxtTelefono.gridx = 1;
							gbc_ftxtTelefono.gridy = 2;
							panel_2.add(ftxtTelefono, gbc_ftxtTelefono);
							
						}
						{
							lblWeb = new JLabel(Messages.getString("Principal.lblWeb.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblWeb = new GridBagConstraints();
							gbc_lblWeb.insets = new Insets(0, 0, 0, 5);
							gbc_lblWeb.anchor = GridBagConstraints.WEST;
							gbc_lblWeb.gridx = 0;
							gbc_lblWeb.gridy = 3;
							panel_2.add(lblWeb, gbc_lblWeb);
						}
						{
							textWeb = new JTextField();
							textWeb.setText("www.carrerasveloces.com"); //$NON-NLS-1$
							textWeb.setEditable(false);
							GridBagConstraints gbc_textWeb = new GridBagConstraints();
							gbc_textWeb.fill = GridBagConstraints.HORIZONTAL;
							gbc_textWeb.gridx = 1;
							gbc_textWeb.gridy = 3;
							panel_2.add(textWeb, gbc_textWeb);
							textWeb.setColumns(10);
						}
					}
					{
						panel_4 = new JPanel();
						panel_4.setBorder(new TitledBorder(null, Messages.getString("Principal.panel_4.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
						GridBagConstraints gbc_panel_4 = new GridBagConstraints();
						gbc_panel_4.gridheight = 2;
						gbc_panel_4.anchor = GridBagConstraints.NORTH;
						gbc_panel_4.fill = GridBagConstraints.HORIZONTAL;
						gbc_panel_4.insets = new Insets(0, 0, 5, 5);
						gbc_panel_4.gridx = 0;
						gbc_panel_4.gridy = 4;
						pDatos.add(panel_4, gbc_panel_4);
						GridBagLayout gbl_panel_4 = new GridBagLayout();
						gbl_panel_4.columnWidths = new int[]{55, 0, 0};
						gbl_panel_4.rowHeights = new int[]{14, 14, 14, 0};
						gbl_panel_4.columnWeights = new double[]{0.0, 1.0, Double.MIN_VALUE};
						gbl_panel_4.rowWeights = new double[]{0.0, 0.0, 0.0, Double.MIN_VALUE};
						panel_4.setLayout(gbl_panel_4);
						{
							lblPremio = new JLabel(Messages.getString("Principal.lblPremio.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblPremio = new GridBagConstraints();
							gbc_lblPremio.anchor = GridBagConstraints.WEST;
							gbc_lblPremio.fill = GridBagConstraints.VERTICAL;
							gbc_lblPremio.insets = new Insets(0, 0, 5, 5);
							gbc_lblPremio.gridx = 0;
							gbc_lblPremio.gridy = 0;
							panel_4.add(lblPremio, gbc_lblPremio);
						}
						{
							txtPremio1 = new JTextField();
							txtPremio1.setText(Messages.getString("Principal.txtPremio1.text")); //$NON-NLS-1$
							txtPremio1.setEditable(false);
							GridBagConstraints gbc_txtPremio1 = new GridBagConstraints();
							gbc_txtPremio1.insets = new Insets(0, 0, 5, 0);
							gbc_txtPremio1.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtPremio1.gridx = 1;
							gbc_txtPremio1.gridy = 0;
							panel_4.add(txtPremio1, gbc_txtPremio1);
							txtPremio1.setColumns(10);
						}
						{
							lblPremio2 = new JLabel(Messages.getString("Principal.lblPremio2.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblPremio2 = new GridBagConstraints();
							gbc_lblPremio2.anchor = GridBagConstraints.WEST;
							gbc_lblPremio2.fill = GridBagConstraints.VERTICAL;
							gbc_lblPremio2.insets = new Insets(0, 0, 5, 5);
							gbc_lblPremio2.gridx = 0;
							gbc_lblPremio2.gridy = 1;
							panel_4.add(lblPremio2, gbc_lblPremio2);
						}
						{
							txtPremio2 = new JTextField();
							txtPremio2.setText(Messages.getString("Principal.txtPremio2.text")); //$NON-NLS-1$
							txtPremio2.setEditable(false);
							txtPremio2.setColumns(10);
							GridBagConstraints gbc_txtPremio2 = new GridBagConstraints();
							gbc_txtPremio2.insets = new Insets(0, 0, 5, 0);
							gbc_txtPremio2.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtPremio2.gridx = 1;
							gbc_txtPremio2.gridy = 1;
							panel_4.add(txtPremio2, gbc_txtPremio2);
						}
						{
							lblPremio3 = new JLabel(Messages.getString("Principal.lblPremio3.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblPremio3 = new GridBagConstraints();
							gbc_lblPremio3.anchor = GridBagConstraints.WEST;
							gbc_lblPremio3.insets = new Insets(0, 0, 0, 5);
							gbc_lblPremio3.fill = GridBagConstraints.VERTICAL;
							gbc_lblPremio3.gridx = 0;
							gbc_lblPremio3.gridy = 2;
							panel_4.add(lblPremio3, gbc_lblPremio3);
						}
						{
							txtPremio3 = new JTextField();
							txtPremio3.setText(Messages.getString("Principal.txtPremio3.text")); //$NON-NLS-1$
							txtPremio3.setEditable(false);
							txtPremio3.setColumns(10);
							GridBagConstraints gbc_txtPremio3 = new GridBagConstraints();
							gbc_txtPremio3.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtPremio3.gridx = 1;
							gbc_txtPremio3.gridy = 2;
							panel_4.add(txtPremio3, gbc_txtPremio3);
						}
					}
					{
						panel_3 = new JPanel();
						panel_3.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), Messages.getString("Principal.panel_3.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null));  //$NON-NLS-1$//$NON-NLS-2$
						GridBagConstraints gbc_panel_3 = new GridBagConstraints();
						gbc_panel_3.gridheight = 2;
						gbc_panel_3.fill = GridBagConstraints.BOTH;
						gbc_panel_3.gridx = 1;
						gbc_panel_3.gridy = 4;
						pDatos.add(panel_3, gbc_panel_3);
						panel_3.setLayout(new BorderLayout(0, 0));
						{
							txtpnDescripcin = new JTextPane();
							txtpnDescripcin.setEditable(false);
							panel_3.add(txtpnDescripcin);
							txtpnDescripcin.setText(Messages.getString("Principal.txtpnDescripcin.text")); //$NON-NLS-1$
						}
					}
				}
				{
					pOrganizaciones = new JPanel();
					tabbedPane.addTab(Messages.getString("Principal.19"), null, pOrganizaciones, null); //$NON-NLS-1$
					GridBagLayout gbl_pOrganizaciones = new GridBagLayout();
					gbl_pOrganizaciones.columnWidths = new int[]{0, 0};
					gbl_pOrganizaciones.rowHeights = new int[]{356, 3, 94, 0, 0};
					gbl_pOrganizaciones.columnWeights = new double[]{1.0, Double.MIN_VALUE};
					gbl_pOrganizaciones.rowWeights = new double[]{0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
					pOrganizaciones.setLayout(gbl_pOrganizaciones);
					{
						lblOrganizadores = new JLabel(""); //$NON-NLS-1$
						lblOrganizadores.setIcon(new ImageIcon(Principal.class.getResource("/resources/Organizadores.png"))); //$NON-NLS-1$
						GridBagConstraints gbc_lblOrganizadores = new GridBagConstraints();
						gbc_lblOrganizadores.insets = new Insets(0, 0, 5, 0);
						gbc_lblOrganizadores.gridx = 0;
						gbc_lblOrganizadores.gridy = 0;
						pOrganizaciones.add(lblOrganizadores, gbc_lblOrganizadores);
					}
					{
						lblColaboradores = new JLabel(Messages.getString("Principal.lblColaboradores.text")); //$NON-NLS-1$
						GridBagConstraints gbc_lblColaboradores = new GridBagConstraints();
						gbc_lblColaboradores.anchor = GridBagConstraints.WEST;
						gbc_lblColaboradores.insets = new Insets(0, 0, 5, 0);
						gbc_lblColaboradores.gridx = 0;
						gbc_lblColaboradores.gridy = 1;
						pOrganizaciones.add(lblColaboradores, gbc_lblColaboradores);
					}
					{
						scrollPane_2 = new JScrollPane();
						GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
						gbc_scrollPane_2.gridheight = 2;
						gbc_scrollPane_2.insets = new Insets(0, 0, 5, 0);
						gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
						gbc_scrollPane_2.gridx = 0;
						gbc_scrollPane_2.gridy = 2;
						pOrganizaciones.add(scrollPane_2, gbc_scrollPane_2);
						{
							list_1 = new JList();
							list_1.setLayoutOrientation(JList.HORIZONTAL_WRAP);
							scrollPane_2.setViewportView(list_1);
						}
					}
				}
				{
					pCartel = new JPanel();
					tabbedPane.addTab(Messages.getString("Principal.20"), null, pCartel, null); //$NON-NLS-1$
					GridBagLayout gbl_pCartel = new GridBagLayout();
					gbl_pCartel.columnWidths = new int[]{425, 0};
					gbl_pCartel.rowHeights = new int[]{466, 0};
					gbl_pCartel.columnWeights = new double[]{0.0, Double.MIN_VALUE};
					gbl_pCartel.rowWeights = new double[]{0.0, Double.MIN_VALUE};
					pCartel.setLayout(gbl_pCartel);
					{
						lblCartel = new JLabel(""); //$NON-NLS-1$
						lblCartel.setIcon(new ImageIcon(Principal.class.getResource("/resources/Cartel.png"))); //$NON-NLS-1$
						GridBagConstraints gbc_lblCartel = new GridBagConstraints();
						gbc_lblCartel.fill = GridBagConstraints.VERTICAL;
						gbc_lblCartel.gridx = 0;
						gbc_lblCartel.gridy = 0;
						pCartel.add(lblCartel, gbc_lblCartel);
					}
				}
				{
					pAdicional = new JPanel();
					tabbedPane.addTab(Messages.getString("Principal.21"), null, pAdicional, null); //$NON-NLS-1$
					GridBagLayout gbl_pAdicional = new GridBagLayout();
					gbl_pAdicional.columnWidths = new int[]{0, 0, 0};
					gbl_pAdicional.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
					gbl_pAdicional.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
					gbl_pAdicional.rowWeights = new double[]{0.0, 1.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
					pAdicional.setLayout(gbl_pAdicional);
					{
						panel_10 = new JPanel();
						panel_10.setBorder(new LineBorder(new Color(0, 0, 0)));
						GridBagConstraints gbc_panel_10 = new GridBagConstraints();
						gbc_panel_10.insets = new Insets(0, 0, 5, 5);
						gbc_panel_10.fill = GridBagConstraints.BOTH;
						gbc_panel_10.gridx = 0;
						gbc_panel_10.gridy = 0;
						pAdicional.add(panel_10, gbc_panel_10);
						{
							lblCmoLlegar = new JLabel(Messages.getString("Principal.lblCmoLlegar.text")); //$NON-NLS-1$
							panel_10.add(lblCmoLlegar);
						}
					}
					{
						panel_9 = new JPanel();
						panel_9.setBorder(new LineBorder(new Color(0, 0, 0)));
						GridBagConstraints gbc_panel_9 = new GridBagConstraints();
						gbc_panel_9.gridwidth = 2;
						gbc_panel_9.insets = new Insets(0, 0, 5, 5);
						gbc_panel_9.fill = GridBagConstraints.BOTH;
						gbc_panel_9.gridx = 0;
						gbc_panel_9.gridy = 1;
						pAdicional.add(panel_9, gbc_panel_9);
						panel_9.setLayout(new BorderLayout(0, 0));
						{
							txtrComoLlegar = new JTextArea();
							txtrComoLlegar.setEditable(false);
							txtrComoLlegar.setText(Messages.getString("Principal.txtrComoLlegar.text")); //$NON-NLS-1$
							panel_9.add(txtrComoLlegar, BorderLayout.CENTER);
						}
					}
					{
						panel_8 = new JPanel();
						panel_8.setBorder(new LineBorder(new Color(0, 0, 0)));
						GridBagConstraints gbc_panel_8 = new GridBagConstraints();
						gbc_panel_8.insets = new Insets(0, 0, 5, 5);
						gbc_panel_8.fill = GridBagConstraints.BOTH;
						gbc_panel_8.gridx = 0;
						gbc_panel_8.gridy = 3;
						pAdicional.add(panel_8, gbc_panel_8);
						{
							lblConsejos = new JLabel(Messages.getString("Principal.lblConsejos.text")); //$NON-NLS-1$
							panel_8.add(lblConsejos);
						}
					}
					{
						panel_7 = new JPanel();
						panel_7.setBorder(new LineBorder(new Color(0, 0, 0)));
						GridBagConstraints gbc_panel_7 = new GridBagConstraints();
						gbc_panel_7.gridwidth = 2;
						gbc_panel_7.fill = GridBagConstraints.BOTH;
						gbc_panel_7.gridx = 0;
						gbc_panel_7.gridy = 4;
						pAdicional.add(panel_7, gbc_panel_7);
						panel_7.setLayout(new BorderLayout(0, 0));
						{
							txtrConvieneDesayunarFuerte = new JTextArea();
							txtrConvieneDesayunarFuerte.setEditable(false);
							txtrConvieneDesayunarFuerte.setText(Messages.getString("Principal.txtrConvieneDesayunarFuerte.text")); //$NON-NLS-1$
							panel_7.add(txtrConvieneDesayunarFuerte);
						}
					}
				}
				{
					pResultados = new JPanel();
					tabbedPane.addTab(Messages.getString("Principal.22"), null, pResultados, null); //$NON-NLS-1$
					GridBagLayout gbl_pResultados = new GridBagLayout();
					gbl_pResultados.columnWidths = new int[]{0, 0, 0, 0, 0};
					gbl_pResultados.rowHeights = new int[]{0, 0, 0, 0, 0};
					gbl_pResultados.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
					gbl_pResultados.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
					pResultados.setLayout(gbl_pResultados);
					{
						panel_5 = new JPanel();
						panel_5.setBorder(new TitledBorder(null, Messages.getString("Principal.panel_5.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
						GridBagConstraints gbc_panel_5 = new GridBagConstraints();
						gbc_panel_5.insets = new Insets(0, 0, 5, 5);
						gbc_panel_5.fill = GridBagConstraints.BOTH;
						gbc_panel_5.gridx = 1;
						gbc_panel_5.gridy = 1;
						pResultados.add(panel_5, gbc_panel_5);
						GridBagLayout gbl_panel_5 = new GridBagLayout();
						gbl_panel_5.columnWidths = new int[]{126, 0, 0};
						gbl_panel_5.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
						gbl_panel_5.columnWeights = new double[]{1.0, 1.0, Double.MIN_VALUE};
						gbl_panel_5.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
						panel_5.setLayout(gbl_panel_5);
						{
							lblTiempo = new JLabel(Messages.getString("Principal.lblTiempo.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblTiempo = new GridBagConstraints();
							gbc_lblTiempo.gridwidth = 2;
							gbc_lblTiempo.insets = new Insets(0, 0, 5, 0);
							gbc_lblTiempo.gridx = 0;
							gbc_lblTiempo.gridy = 0;
							panel_5.add(lblTiempo, gbc_lblTiempo);
						}
						{
							frmtdtxtfldTiempo = new JFormattedTextField();
							frmtdtxtfldTiempo.setEditable(false);
							frmtdtxtfldTiempo.setText(Messages.getString("Principal.frmtdtxtfldTiempo.text")); //$NON-NLS-1$
							GridBagConstraints gbc_frmtdtxtfldTiempo = new GridBagConstraints();
							gbc_frmtdtxtfldTiempo.gridwidth = 2;
							gbc_frmtdtxtfldTiempo.insets = new Insets(0, 0, 5, 0);
							gbc_frmtdtxtfldTiempo.fill = GridBagConstraints.HORIZONTAL;
							gbc_frmtdtxtfldTiempo.gridx = 0;
							gbc_frmtdtxtfldTiempo.gridy = 1;
							panel_5.add(frmtdtxtfldTiempo, gbc_frmtdtxtfldTiempo);
						}
						{
							lblNParticipantes = new JLabel(Messages.getString("Principal.lblNParticipantes.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblNParticipantes = new GridBagConstraints();
							gbc_lblNParticipantes.anchor = GridBagConstraints.EAST;
							gbc_lblNParticipantes.insets = new Insets(0, 0, 5, 5);
							gbc_lblNParticipantes.gridx = 0;
							gbc_lblNParticipantes.gridy = 3;
							panel_5.add(lblNParticipantes, gbc_lblNParticipantes);
						}
						
						{
							MaskFormatter formatoNPart;
							try {
								formatoNPart = new MaskFormatter("####'"); //$NON-NLS-1$
								formatoNPart.setPlaceholderCharacter(' ');
								ftxtNParticipantes = new JFormattedTextField(formatoNPart);
							}catch (ParseException e){
								e.printStackTrace();
							}
							
							ftxtNParticipantes.setText("98"); //$NON-NLS-1$
							ftxtNParticipantes.setEditable(false);
							GridBagConstraints gbc_ftxtNParticipantes = new GridBagConstraints();
							gbc_ftxtNParticipantes.insets = new Insets(0, 0, 5, 0);
							gbc_ftxtNParticipantes.fill = GridBagConstraints.HORIZONTAL;
							gbc_ftxtNParticipantes.gridx = 1;
							gbc_ftxtNParticipantes.gridy = 3;
							panel_5.add(ftxtNParticipantes, gbc_ftxtNParticipantes);
						}
						{
							lblNAbandonos = new JLabel(Messages.getString("Principal.lblNAbandonos.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblNAbandonos = new GridBagConstraints();
							gbc_lblNAbandonos.anchor = GridBagConstraints.EAST;
							gbc_lblNAbandonos.insets = new Insets(0, 0, 0, 5);
							gbc_lblNAbandonos.gridx = 0;
							gbc_lblNAbandonos.gridy = 4;
							panel_5.add(lblNAbandonos, gbc_lblNAbandonos);
						}
						{
							MaskFormatter formatoNAband;
							try {
								formatoNAband = new MaskFormatter("#####'"); //$NON-NLS-1$
								formatoNAband.setPlaceholderCharacter(' ');
								ftxtNAbandonos = new JFormattedTextField(formatoNAband);
							}catch (ParseException e){
								e.printStackTrace();
							}
							
							ftxtNAbandonos.setText("15"); //$NON-NLS-1$
							ftxtNAbandonos.setEditable(false);
							GridBagConstraints gbc_ftxtNAbandonos = new GridBagConstraints();
							gbc_ftxtNAbandonos.fill = GridBagConstraints.HORIZONTAL;
							gbc_ftxtNAbandonos.gridx = 1;
							gbc_ftxtNAbandonos.gridy = 4;
							panel_5.add(ftxtNAbandonos, gbc_ftxtNAbandonos);
						}
					}
					{
						panel_6 = new JPanel();
						panel_6.setBorder(new TitledBorder(null, Messages.getString("Principal.panel_6.borderTitle"), TitledBorder.LEADING, TitledBorder.TOP, null, null)); //$NON-NLS-1$
						GridBagConstraints gbc_panel_6 = new GridBagConstraints();
						gbc_panel_6.insets = new Insets(0, 0, 5, 5);
						gbc_panel_6.fill = GridBagConstraints.BOTH;
						gbc_panel_6.gridx = 2;
						gbc_panel_6.gridy = 1;
						pResultados.add(panel_6, gbc_panel_6);
						GridBagLayout gbl_panel_6 = new GridBagLayout();
						gbl_panel_6.columnWidths = new int[]{18, 36, 0, 0};
						gbl_panel_6.rowHeights = new int[]{0, 14, 0, 0, 0, 0};
						gbl_panel_6.columnWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
						gbl_panel_6.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
						panel_6.setLayout(gbl_panel_6);
						{
							lblPrimero = new JLabel(Messages.getString("Principal.lblPrimero.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblPrimero = new GridBagConstraints();
							gbc_lblPrimero.insets = new Insets(0, 0, 5, 5);
							gbc_lblPrimero.anchor = GridBagConstraints.NORTHEAST;
							gbc_lblPrimero.gridx = 1;
							gbc_lblPrimero.gridy = 1;
							panel_6.add(lblPrimero, gbc_lblPrimero);
						}
						{
							txtPrimero = new JTextField();
							txtPrimero.setEditable(false);
							txtPrimero.setText("Fernández Verne, Luisa"); //$NON-NLS-1$
							GridBagConstraints gbc_txtPrimero = new GridBagConstraints();
							gbc_txtPrimero.insets = new Insets(0, 0, 5, 0);
							gbc_txtPrimero.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtPrimero.gridx = 2;
							gbc_txtPrimero.gridy = 1;
							panel_6.add(txtPrimero, gbc_txtPrimero);
							txtPrimero.setColumns(10);
						}
						{
							lblSegundo = new JLabel(Messages.getString("Principal.lblSegundo.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblSegundo = new GridBagConstraints();
							gbc_lblSegundo.anchor = GridBagConstraints.EAST;
							gbc_lblSegundo.insets = new Insets(0, 0, 5, 5);
							gbc_lblSegundo.gridx = 1;
							gbc_lblSegundo.gridy = 2;
							panel_6.add(lblSegundo, gbc_lblSegundo);
						}
						{
							txtSegundo = new JTextField();
							txtSegundo.setEditable(false);
							txtSegundo.setText("Ramírez Ramos, Ramón"); //$NON-NLS-1$
							GridBagConstraints gbc_txtSegundo = new GridBagConstraints();
							gbc_txtSegundo.insets = new Insets(0, 0, 5, 0);
							gbc_txtSegundo.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtSegundo.gridx = 2;
							gbc_txtSegundo.gridy = 2;
							panel_6.add(txtSegundo, gbc_txtSegundo);
							txtSegundo.setColumns(10);
						}
						{
							lblTercero = new JLabel(Messages.getString("Principal.lblTercero.text")); //$NON-NLS-1$
							GridBagConstraints gbc_lblTercero = new GridBagConstraints();
							gbc_lblTercero.anchor = GridBagConstraints.EAST;
							gbc_lblTercero.insets = new Insets(0, 0, 5, 5);
							gbc_lblTercero.gridx = 1;
							gbc_lblTercero.gridy = 3;
							panel_6.add(lblTercero, gbc_lblTercero);
						}
						{
							txtTercero = new JTextField();
							txtTercero.setEditable(false);
							txtTercero.setText("Toledo Alarcos, Manuel"); //$NON-NLS-1$
							GridBagConstraints gbc_txtTercero = new GridBagConstraints();
							gbc_txtTercero.insets = new Insets(0, 0, 5, 0);
							gbc_txtTercero.fill = GridBagConstraints.HORIZONTAL;
							gbc_txtTercero.gridx = 2;
							gbc_txtTercero.gridy = 3;
							panel_6.add(txtTercero, gbc_txtTercero);
							txtTercero.setColumns(10);
						}
					}
					{
						scrollPane_1 = new JScrollPane();
						scrollPane_1.setBorder(new LineBorder(new Color(130, 135, 144)));
						GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
						gbc_scrollPane_1.gridwidth = 2;
						gbc_scrollPane_1.insets = new Insets(0, 0, 5, 5);
						gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
						gbc_scrollPane_1.gridx = 1;
						gbc_scrollPane_1.gridy = 3;
						pResultados.add(scrollPane_1, gbc_scrollPane_1);
						{
							list_2 = new JList();
							scrollPane_1.setViewportView(list_2);
						}
					}
				}
			}
			{
				panelCompeticiones = new JPanel();
				GridBagConstraints gbc_panelCompeticiones = new GridBagConstraints();
				gbc_panelCompeticiones.gridwidth = 2;
				gbc_panelCompeticiones.gridheight = 2;
				gbc_panelCompeticiones.insets = new Insets(0, 0, 5, 5);
				gbc_panelCompeticiones.fill = GridBagConstraints.BOTH;
				gbc_panelCompeticiones.gridx = 0;
				gbc_panelCompeticiones.gridy = 0;
				panelPrincipal.add(panelCompeticiones, gbc_panelCompeticiones);
				GridBagLayout gbl_panelCompeticiones = new GridBagLayout();
				gbl_panelCompeticiones.columnWidths = new int[]{63, 75, 63, 0};
				gbl_panelCompeticiones.rowHeights = new int[]{0, 290, 40, 0};
				gbl_panelCompeticiones.columnWeights = new double[]{1.0, 0.0, 1.0, Double.MIN_VALUE};
				gbl_panelCompeticiones.rowWeights = new double[]{1.0, 1.0, 0.0, Double.MIN_VALUE};
				panelCompeticiones.setLayout(gbl_panelCompeticiones);
				{
					lblCompeticiones = new JLabel(Messages.getString("Principal.lblCompeticiones.text")); //$NON-NLS-1$
					GridBagConstraints gbc_lblCompeticiones = new GridBagConstraints();
					gbc_lblCompeticiones.anchor = GridBagConstraints.WEST;
					gbc_lblCompeticiones.gridwidth = 2;
					gbc_lblCompeticiones.insets = new Insets(0, 0, 5, 5);
					gbc_lblCompeticiones.gridx = 0;
					gbc_lblCompeticiones.gridy = 0;
					panelCompeticiones.add(lblCompeticiones, gbc_lblCompeticiones);
				}
				{
					scrollPane = new JScrollPane();
					GridBagConstraints gbc_scrollPane = new GridBagConstraints();
					gbc_scrollPane.gridwidth = 3;
					gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
					gbc_scrollPane.fill = GridBagConstraints.BOTH;
					gbc_scrollPane.gridx = 0;
					gbc_scrollPane.gridy = 1;
					panelCompeticiones.add(scrollPane, gbc_scrollPane);
					{
						lstCompeticiones = new JList();
						lstCompeticiones.setToolTipText(Messages.getString("Principal.lstCompeticiones.toolTipText")); //$NON-NLS-1$
						lstCompeticiones.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
						lstCompeticiones.setCellRenderer(new MiListCellRenderer());
						lstCompeticiones.addListSelectionListener(new LstCompeticionesListSelectionListener());
						modeloListaComp = new DefaultListModel();
						lstCompeticiones.setModel(modeloListaComp);
								
						/*{ lstCompeticiones.setModel(new AbstractModel({
							String[] values = new String[] {"Maraton Ciudad Real", "Carrera Bianual Local"};
							public int getSize() {
								return values.length;
							}
							public Object getElementAt(int index) {
								return values[index];
							}
						});
						*/
						modeloListaComp.addElement("Maraton Ciudad Real"); //$NON-NLS-1$
						modeloListaComp.addElement("Carrera Bianual Local"); //$NON-NLS-1$
						lstCompeticiones.setSelectedIndex(0);
						scrollPane.setViewportView(lstCompeticiones);
					}
				}
				{
					btnNuevo = new JButton(Messages.getString("Principal.btnNuevo.text")); //$NON-NLS-1$
					btnNuevo.setToolTipText(Messages.getString("Principal.btnNuevo.toolTipText")); //$NON-NLS-1$
					btnNuevo.addActionListener(new BtnNuevoActionListener());
					GridBagConstraints gbc_btnNuevo = new GridBagConstraints();
					gbc_btnNuevo.anchor = GridBagConstraints.NORTH;
					gbc_btnNuevo.insets = new Insets(0, 0, 0, 5);
					gbc_btnNuevo.gridx = 0;
					gbc_btnNuevo.gridy = 2;
					panelCompeticiones.add(btnNuevo, gbc_btnNuevo);
				}
				{
					btnModificar = new JToggleButton(Messages.getString("Principal.btnModificar.text")); //$NON-NLS-1$
					btnModificar.setToolTipText(Messages.getString("Principal.btnModificar.toolTipText")); //$NON-NLS-1$
					btnModificar.addActionListener(new BtnModificarActionListener());
					GridBagConstraints gbc_btnModificar = new GridBagConstraints();
					gbc_btnModificar.anchor = GridBagConstraints.NORTHWEST;
					gbc_btnModificar.insets = new Insets(0, 0, 0, 5);
					gbc_btnModificar.gridx = 1;
					gbc_btnModificar.gridy = 2;
					panelCompeticiones.add(btnModificar, gbc_btnModificar);
				}
				{
					btnBorrar = new JButton(Messages.getString("Principal.btnBorrar.text")); //$NON-NLS-1$
					btnBorrar.setToolTipText(Messages.getString("Principal.btnBorrar.toolTipText")); //$NON-NLS-1$
					btnBorrar.addActionListener(new BtnBorrarActionListener());
					GridBagConstraints gbc_btnBorrar = new GridBagConstraints();
					gbc_btnBorrar.anchor = GridBagConstraints.NORTH;
					gbc_btnBorrar.gridx = 2;
					gbc_btnBorrar.gridy = 2;
					panelCompeticiones.add(btnBorrar, gbc_btnBorrar);
				}
			}
			{
				btnMapa = new JButton(Messages.getString("Principal.btnMapa.text")); //$NON-NLS-1$
				btnMapa.setToolTipText(Messages.getString("Principal.btnMapa.toolTipText")); //$NON-NLS-1$
				btnMapa.addActionListener(new BtnMapaActionListener());
				btnMapa.setIcon(new ImageIcon(Principal.class.getResource("/resources/Map-icon.png"))); //$NON-NLS-1$
				GridBagConstraints gbc_btnMapa = new GridBagConstraints();
				gbc_btnMapa.gridheight = 2;
				gbc_btnMapa.insets = new Insets(0, 0, 0, 5);
				gbc_btnMapa.gridx = 0;
				gbc_btnMapa.gridy = 2;
				panelPrincipal.add(btnMapa, gbc_btnMapa);
			}
			{
				btnParticipantes = new JButton(Messages.getString("Principal.btnParticipantes.text")); //$NON-NLS-1$
				btnParticipantes.setToolTipText(Messages.getString("Principal.btnParticipantes.toolTipText")); //$NON-NLS-1$
				GridBagConstraints gbc_btnParticipantes = new GridBagConstraints();
				gbc_btnParticipantes.gridheight = 2;
				gbc_btnParticipantes.insets = new Insets(0, 0, 0, 5);
				gbc_btnParticipantes.gridx = 1;
				gbc_btnParticipantes.gridy = 2;
				panelPrincipal.add(btnParticipantes, gbc_btnParticipantes);
				btnParticipantes.setIcon(new ImageIcon(Principal.class.getResource("/resources/Man___woman_running_normal.jpg"))); //$NON-NLS-1$
				btnParticipantes.addActionListener(new BtnParticipantesActionListener());
			}
		}
	}

	private class BtnCerrarSesinActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0){
			Login window = new Login();
			window.getFrame().setVisible(true);
			dispose();
			
		}
	}
	private class BtnParticipantesActionListener implements ActionListener {
		
		public void actionPerformed(ActionEvent arg0) {
			Participantes ventanaParticipantes = new Participantes();
			//se hace visible
			ventanaParticipantes.setVisible(true);
			//se destruye la ventana actual (atributo a nivel de clase)
		}
	}
	private class BtnMapaActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			Mapa ventanaMapa = new Mapa();
			//se hace visible
			ventanaMapa.setVisible(true);
		}
	}
	private class LstCompeticionesListSelectionListener implements ListSelectionListener {
		public void valueChanged(ListSelectionEvent e) {
			try{
			boolean modificando = btnModificar.isSelected();
			btnModificar.setSelected(true);

			switch (lstCompeticiones.getSelectedIndex()){
			case 0:
				cargarCarrera1();
				break;
			case 1:
				cargarCarrera2();
				break;
			}
			if (!modificando) btnModificar.setSelected(false);
			}catch (Exception exc){
				
			}
		}
	}
	private class BtnModificarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			boolean estado = btnModificar.isSelected();
			 txtPremio1.setEditable(estado);
			 txtPremio2.setEditable(estado);
			 txtPremio3.setEditable(estado);
			 ftxtHora.setEditable(estado);
			 txtDistancia.setEditable(estado);
			 txtpnDescripcin.setEditable(estado);
			 frmtdtxtfldTiempo.setEditable(estado);
			 txtrConvieneDesayunarFuerte.setEditable(estado);
			 txtrComoLlegar.setEditable(estado);
			 txtPrimero.setEditable(estado);
			 txtSegundo.setEditable(estado);
			 txtTercero.setEditable(estado);
			 txtLocalidad.setEditable(estado);
			 textEmail.setEditable(estado);
			 textWeb.setEditable(estado);
			 ftxtNParticipantes.setEditable(estado);
			 ftxtNAbandonos.setEditable(estado);
			 ftxtCoste_1.setEditable(estado);
			 ftxtTelefono.setEditable(estado);
			 ftxtPlazas.setEditable(estado);
			 chckFinalizada.setEnabled(estado);
			 
		}
	}
	private class FtxtHoraFocusListener extends FocusAdapter {
		@Override
		public void focusLost(FocusEvent e) {
			try{
			String [] partes = ftxtHora.getText().split(":"); //$NON-NLS-1$
			int horas = Integer.parseInt(partes[0]);
			int minutos = Integer.parseInt(partes[1]);
			if(horas > 23 || minutos > 60){
				ftxtHora.setText(""); //$NON-NLS-1$
				JOptionPane.showMessageDialog(null, Messages.getString("Principal.23") //$NON-NLS-1$
						+ Messages.getString("Principal.24"),  //$NON-NLS-1$
						Messages.getString("Principal.25"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
			}
			}catch(Exception exc){
				
			};
		}
	}
	private class BtnNuevoActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			String nombre = ""; //$NON-NLS-1$
			nombre = JOptionPane.showInputDialog(null, 
					Messages.getString("Principal.26"),  //$NON-NLS-1$
					Messages.getString("Principal.27"), JOptionPane.QUESTION_MESSAGE); //$NON-NLS-1$
				
			if(nombre == null || nombre.equals("")){ //$NON-NLS-1$
				JOptionPane.showMessageDialog(null, Messages.getString("Principal.28") //$NON-NLS-1$
						+ Messages.getString("Principal.29"),  //$NON-NLS-1$
						Messages.getString("Principal.30"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$
				}
			else modeloListaComp.addElement(nombre);
				
			}
	}
	
	private class BtnBorrarActionListener implements ActionListener {
		public void actionPerformed(ActionEvent e) {
		  if(lstCompeticiones.getSelectedValue()!=null){
			if(JOptionPane.showConfirmDialog(null, Messages.getString("Principal.31") //$NON-NLS-1$
					+ " " + lstCompeticiones.getSelectedValue() +"?",Messages.getString("Principal.32"), //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					JOptionPane.YES_NO_OPTION ) == 0){
				modeloListaComp.remove(lstCompeticiones.getSelectedIndex());
				JOptionPane.showMessageDialog(null, Messages.getString("Principal.33") //$NON-NLS-1$
					+ Messages.getString("Principal.34"), Messages.getString("Principal.35"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}
			else{
				JOptionPane.showMessageDialog(null, Messages.getString("Principal.36") //$NON-NLS-1$
					+ Messages.getString("Principal.37"), Messages.getString("Principal.38"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
			}
		  }
		  else  JOptionPane.showMessageDialog(null, Messages.getString("Principal.39") //$NON-NLS-1$
				+ Messages.getString("Principal.40"),  //$NON-NLS-1$
				Messages.getString("Principal.41"), JOptionPane.WARNING_MESSAGE); //$NON-NLS-1$
		}
	}
	private class MiComboBoxActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			try{
				if (!btnModificar.isSelected() && ((JComboBox) arg0.getSource()).getSelectedIndex()!=seleccion){
				((JComboBox) arg0.getSource()).setSelectedIndex(seleccion);
				JOptionPane.showMessageDialog(null,Messages.getString("Principal.42") //$NON-NLS-1$
						+ Messages.getString("Principal.43"), Messages.getString("Principal.44"),JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
				}
			}catch (Exception exc){
				
			}
			
		}
	}
	private class MiComboBoxFocusListener extends FocusAdapter {
		@Override
		public void focusGained(FocusEvent e) {
			seleccion = ((JComboBox) e.getSource()).getSelectedIndex();
		}
	}
	private class MntmAcercaDeActionListener implements ActionListener {
		public void actionPerformed(ActionEvent arg0) {
			JOptionPane.showMessageDialog(null, Messages.getString("Principal.63"), Messages.getString("Principal.64"), JOptionPane.INFORMATION_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
		}
	}
	
	public void cargarCarrera1(){
		 txtPremio1.setText("200 €"); //$NON-NLS-1$
		 txtPremio2.setText("100 €"); //$NON-NLS-1$
		 txtPremio3.setText(Messages.getString("Principal.45")); //$NON-NLS-1$
		 ftxtHora.setText("0830"); //$NON-NLS-1$
		 cbDificultad.setSelectedItem(Messages.getString("Principal.46")); //$NON-NLS-1$
		 cbModalidad.setSelectedItem(Messages.getString("Principal.47")); //$NON-NLS-1$
		 txtDistancia.setText("6 km"); //$NON-NLS-1$
		 txtpnDescripcin.setText(Messages.getString("Principal.48")); //$NON-NLS-1$
		 frmtdtxtfldTiempo.setText(Messages.getString("Principal.49")); //$NON-NLS-1$
		 txtrConvieneDesayunarFuerte.setText(Messages.getString("Principal.50")); //$NON-NLS-1$
		 txtrComoLlegar.setText(Messages.getString("Principal.51")); //$NON-NLS-1$
		 txtPrimero.setText("Fernández Verne, Luisa"); //$NON-NLS-1$
		 txtSegundo.setText("Alberto Martínez Melchor"); //$NON-NLS-1$
		 txtTercero.setText("Ramírez Ramos, Ramón"); //$NON-NLS-1$
		 txtLocalidad.setText("Ciudad Real"); //$NON-NLS-1$
		 textEmail.setText("carreras@veloces.com"); //$NON-NLS-1$
		 textWeb.setText("www.carrerasveloces.com"); //$NON-NLS-1$
		 ftxtNParticipantes.setText("98"); //$NON-NLS-1$
		 ftxtNAbandonos.setText("15"); //$NON-NLS-1$
		 ftxtCoste_1.setText("15"); //$NON-NLS-1$
		 ftxtTelefono.setText("034 612 345 678"); //$NON-NLS-1$
		 ftxtPlazas.setText("90"); //$NON-NLS-1$
		 cbDiaInscr.setSelectedItem("4"); //$NON-NLS-1$
		 cbMesInscr.setSelectedItem(Messages.getString("Principal.52")); //$NON-NLS-1$
		 cbAnioInscr.setSelectedItem("2015"); //$NON-NLS-1$
		 lblOrganizadores.setIcon(new ImageIcon(Principal.class.getResource("/resources/Organizadores.png"))); //$NON-NLS-1$
		 lblCartel.setIcon(new ImageIcon(Principal.class.getResource("/resources/Cartel.png"))); //$NON-NLS-1$
		 chckFinalizada.setSelected(false);
	}
	public void cargarCarrera2(){
		 txtPremio1.setText("200 €"); //$NON-NLS-1$
		 txtPremio2.setText(Messages.getString("Principal.53")); //$NON-NLS-1$
		 txtPremio3.setText(Messages.getString("Principal.54")); //$NON-NLS-1$
		 ftxtHora.setText("1815"); //$NON-NLS-1$
		 cbDificultad.setSelectedItem(Messages.getString("Principal.55")); //$NON-NLS-1$
		 cbModalidad.setSelectedItem(Messages.getString("Principal.56")); //$NON-NLS-1$
		 txtDistancia.setText("5 km"); //$NON-NLS-1$
		 txtpnDescripcin.setText(Messages.getString("Principal.57")); //$NON-NLS-1$
		 frmtdtxtfldTiempo.setText(Messages.getString("Principal.58")); //$NON-NLS-1$
		 txtrConvieneDesayunarFuerte.setText(Messages.getString("Principal.59")); //$NON-NLS-1$
		 txtrComoLlegar.setText(Messages.getString("Principal.60")); //$NON-NLS-1$
		 txtPrimero.setText("Borja Ruiz López"); //$NON-NLS-1$
		 txtSegundo.setText("Laura Herencia Martín"); //$NON-NLS-1$
		 txtTercero.setText("Paqui Ocaña García"); //$NON-NLS-1$
		 txtLocalidad.setText("Miguelturra"); //$NON-NLS-1$
		 textEmail.setText("estelares@resistentes.es"); //$NON-NLS-1$
		 textWeb.setText("www.fugaces.es"); //$NON-NLS-1$
		 ftxtNParticipantes.setText("212"); //$NON-NLS-1$
		 ftxtNAbandonos.setText("4"); //$NON-NLS-1$
		 ftxtCoste_1.setText("20"); //$NON-NLS-1$
		 ftxtTelefono.setText("034 619 876 543"); //$NON-NLS-1$
		 ftxtPlazas.setText("90"); //$NON-NLS-1$
		 cbDiaInscr.setSelectedItem("3"); //$NON-NLS-1$
		 cbMesInscr.setSelectedItem(Messages.getString("Principal.61")); //$NON-NLS-1$
		 cbAnioInscr.setSelectedItem("2015"); //$NON-NLS-1$
		 lblOrganizadores.setIcon(new ImageIcon(Principal.class.getResource("/resources/Organizadores1.png"))); //$NON-NLS-1$
		 lblCartel.setIcon(new ImageIcon(Principal.class.getResource("/resources/Cartel1.png"))); //$NON-NLS-1$
		 chckFinalizada.setSelected(true);
	}
}
